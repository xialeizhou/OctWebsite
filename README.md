OctWebsite
==========

## This is a OctoWebsite.
The website engine is Octopress, a popular open source framework from rails community.

Octopress is a framework designed by for Jekyll, the blog aware static site generator powering Github Pages.

I host my website on heroku.

## 中文说明
本个人站点基于Octopress开源框架，网站运行采用了基于[Heroku](http://heroku.com)主机托管解决方案。
Octopress框架是rails社区最受欢迎的博客引擎, 支持以markrdown格式来post article.

Octopress本身基于Jekyll来生成静态页面, 然后push到[Github](https://github.com),远程仓库作为网站运行的后台服务器。

注： [Rails](http://rubyonrails.org/)由[ruby](https://www.ruby-lang.org)语言编写，是一个基于MVC设计模式的高效的Web开发框架。

## 所需技术
- Basic [ruby](https://www.ruby-lang.org) programming skill

- Familar [Rails](http://rubyonrails.org/) framweork

- javascript, css skil

- [Jekyll](http://jekyllrb.com/) & [Octopress](http://octopress.org) framework's apis

- akefile & Gemfile syntax

- Familar how to host your apps on [Heroku](http://heroku.com)

- Basic [Github](https://github.com) knowlege

- Generate your public rsa keys [forgithub ](https://help.github.com/articles/generating-ssh-keys) && [for heroku](ttps://devcenter.heroku.com/articles/keys)

- [Jekyll](http://jekyllrb.com/) & [Octopress](http://octopress.org) framework's apis
  
- akefile & Gemfile syntax
  
- Familar how to host your apps on [Heroku](http://heroku.com)
  
- Basic [Github](https://github.com) knowlege
  
- Generate your public rsa keys [forgithub ](https://help.github.com/articles/generating-ssh-keys) && [for heroku](ttps://devcenter.heroku.com/articles/keys)

- [Markdown](http://en.wikipedia.org/wiki/Markdown) syntax

## 参考
- 个人站点: http://www.luciux.com.
- Check out [Octopress.org](http://octopress.org) for guides and documentation.
- Clone [octopress.git](https://github.com/imathis/octopress) to start build your webside.

