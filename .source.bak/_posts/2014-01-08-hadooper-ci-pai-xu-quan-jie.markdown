---
layout: post
title: "hadoop二次排序全解"
date: 2014-01-08 03:31:29 +0800
comments: true
categories: [hadoop]
tage: [hadoop]
---
关于二次排序主要涉及到：

> job.setPartitionerClass(Partitioner p);
> job.setSortComparatorClass(RawComparator c);
> job.setGroupingComparatorClass(RawComparator c);

注意这三个默认的类。WritableComparator是RawComparator的实现接口。hadoop所有实现的可序列化类，比如IntWritable,LongWritable,Text,都实现了一个内部类：
{% codeblock lang:ruby %}
public static class Comparator extends WritableComparator {
  public Comparator() {
    super(A.class);
  }
{% endcodeblock %}
A是类名。所以所有的map输出key的默认比较行为都是被定义在WritableComparator里的。当然可以通过自定义自己的GroupingComparatorClass，SortComparatorClass等来实现用户的特殊排序，>聚合和分区需求。

二次排序
------
 在Hadoop中，默认情况下是按照key进行排序，如果要按照value进行排序怎么办？即：对于同一个key，reduce函数接收到的value list是按照value排序的。这种应用需求在join操作中很常见，>比如，希望相同的key中，小表对应的value排在前面。

有两种方法进行二次排序，分别为：**buffer and in memory sort**和 **value-to-key conversion**。

对于**buffer and in memory sort**，主要思想是：
>    在reduce()函数中，将某个key对应的所有value保存下来，然后进行排序。 这种方法最大的缺点是：可能会造成out of memory。

对于**value-to-key conversion**，主要思想是：

>    将key和部分value拼接成一个组合key（实现WritableComparable接口或者调用setSortComparatorClass函数），这样reduce获取的结果便是先按key排序，后按value排序的结果。

需要注意的是，用户需要自己实现Paritioner，以便只按照key进行数据划分。

Hadoop显式的支持二次排序，在Configuration类中有个setGroupingComparatorClass()方法，可用于设置排序group的key值，具体参考：[here](http://my.oschina.net/leejun2005/blog/132785).

