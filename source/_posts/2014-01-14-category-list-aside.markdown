---
layout: post
title: "Category List Aside"
date: 2014-01-14 00:28:56 +0800
comments: true
categories: [coding]
keywords: [coding]
---

## Category List Plug-in

opy the CategoryListTag module in plugins/category_list_tag.rb. Here is a copy of Dan’s code:

```ruby plugins/category_list_tag.rb
module Jekyll
  class CategoryListTag < Liquid::Tag
    def render(context)
      html = ""
      categories = context.registers[:site].categories.keys
      categories.sort.each do |category|
        posts_in_category = context.registers[:site].categories[category].size
        category_dir = context.registers[:site].config['category_dir']
        category_url = File.join(category_dir, category.gsub(/_|\P{Word}/, '-').gsub(/-{2,}/, '-').downcase)
        html << "<li class='category'><a href='/#{category_url}/'>#{category} (#{posts_in_category})</a></li>\n"
      end
      html
    end
  end
end

Liquid::Template.register_tag('category_list', Jekyll::CategoryListTag)
```
## The Page

Alright ! Now all we have to do is create a page (mine is in source/blog/categories/index.html), give it the category_list liquid tag and voilà ! Here is a quick example:

```html source/blog/categories/index.html
---
layout: page
title: "Blog Categories"
comments: false
footer: true
---

<div id="blog-categories">
  <ul>
    {% category_list %}
  </ul> </div> 
```
## Category List Aside 

awesome category list will:

*  display all your categories with a post counter (using Bootstrap’s badge class)
*  highlight when you’re on the matching category page
*  be installed in 3 minutes !

## The Trick

  `<section class="panel panel-default">`
  `<div class="panel-heading">`
    `<h3 class="panel-title">Categories</h3>`
  `</div>`
  `<div class="list-group">`
    `{% for category in site.categories %}`
    `{% capture category_url %}{{ site.category_dir }}/{{ category | first | slugize | downcase | replace:' ','-' | append:'/index.html'}}{% endcapture %}`
    `<a class="list-group-item {% if category_url == page.url %}active{% endif %}" href="{{ root_url | append:'/' | append:category_url }}">`
        `<span class="badge">{{ category | last | size }}</span>`
        `{{ category | first }}`
      `</a>`
    `{% endfor %}`
  `</div>`
`</section>`

Put it in the source/_includes/custom/asides directory and then adapt your _config.yml accordingly. As example, here is how it is done on this site:

```ruby _config.yml

# ...
# list each of the sidebar modules you want to include, in the order you want them to appear.
# To add custom asides, create files in /source/_includes/custom/asides/ and add them to the list like 'custom/asides/custom_aside_name.html'
default_asides: [
    asides/recent_posts.html,
    custom/asides/category_list.html
]

# Each layout uses the default asides, but they can have their own asides instead. Simply uncomment the lines below
# and add an array with the asides you want to use.
blog_index_asides: [
    asides/what_is_octostrap3.html,
    asides/recent_posts.html,
    custom/asides/category_list.html,
    asides/github.html
]
# post_asides:
# page_asides:
# ...
```
