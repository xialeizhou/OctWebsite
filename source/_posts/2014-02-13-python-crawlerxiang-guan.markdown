---
layout: post
title: "python crawler相关"
date: 2014-02-13 10:18:47 +0800
comments: true
categories: [python]
---

搜集用于制作个人及企业级爬虫的相关文档

# 一.official documents

## urllib — Open arbitrary resources by URL

This module provides a high-level interface for fetching data across the World Wide Web. In particular, the urlopen() function is similar to the built-in function open(), but accepts Universal Resource Locators (URLs) instead of filenames. Some restrictions apply — it can only open URLs for reading, and no seek operations are available.

doc: [http://docs.python.org/2/library/urllib.html](http://docs.python.org/2/library/urllib.html)

## urllib2 — extensible library for opening URLs

The urllib2 module defines functions and classes which help in opening URLs (mostly HTTP) in a complex world — basic and digest authentication, redirections, cookies and more.

doc:  [http://docs.python.org/2/library/urllib2.html](http://docs.python.org/2/library/urllib2.html)

## Selenium


Selenium is a portable software testing framework for web applications. Selenium provides a record/playback tool for authoring tests without learning a test scripting language (Selenium IDE). It also provides a test domain-specific language (Selenese)[1] to write tests in a number of popular programming languages, including Java, C#, Groovy, Perl, PHP, Python and Ruby. The tests can then be run against most modern web browsers. Selenium deploys on Windows, Linux, and Macintosh platforms.

doc:  [http://docs.seleniumhq.org/docs/](http://docs.seleniumhq.org/docs/)

## Beautiful Soup

Beautiful Soup is a Python library for pulling data out of HTML and XML files. It works with your favorite parser to provide idiomatic ways of navigating, searching, and modifying the parse tree. It commonly saves programmers hours or days of work.
{% img right http://www.crummy.com/software/BeautifulSoup/bs4/doc/_images/6.1.jpg %}

These instructions illustrate all major features of Beautiful Soup 4, with examples. I show you what the library is good for, how it works, how to use it, how to make it do what you want, and what to do when it violates your expectations.


The examples in this documentation should work the same way in Python 2.7 and Python 3.2.

You might be looking for the documentation for Beautiful Soup 3. If so, you should know that Beautiful Soup 3 is no longer being developed, and that Beautiful Soup 4 is recommended for all new projects. If you want to learn about the differences between Beautiful Soup 3 and Beautiful Soup 4, see Porting code to BS4.

This documentation has been translated into other languages by Beautiful Soup users.


doc: [http://www.crummy.com/software/BeautifulSoup/bs4/doc/](http://www.crummy.com/software/BeautifulSoup/bs4/doc/)

## pyquery  — a jquery-like library for python

pyquery allows you to make jquery queries on xml documents. The API is as much as possible the similar to jquery. pyquery uses lxml for fast xml and html manipulation.

doc: [http://pythonhosted.org/pyquery/index.html](http://pythonhosted.org/pyquery/index.html)

## Scrapy 

Scrapy is a fast high-level screen scraping and web crawling framework, used to crawl websites and extract structured data from their pages. It can be used for a wide range of purposes, from data mining to monitoring and automated testing.

doc: [http://doc.scrapy.org/en/latest/](http://doc.scrapy.org/en/latest/)

## requests —  HTTP for Humans

Requests 是使用 Apache2 Licensed 许可证的 HTTP 库。用 Python 编写，为人类编写。

Python 标准库中的 urllib2 模块提供了你所需要的大多数 HTTP 功能，但是它的 API 烂出翔来了。它是为另一个时代、另一个互联网所创建的。它需要巨量的工作，甚至包括各种方法覆盖，来完成最简单的任务。

在Python的世界里，事情不应该这么麻烦。

doc: [http://cn.python-requests.org/en/latest/](http://cn.python-requests.org/en/latest/)

## Ghost 

ghost.py is a webkit web client written in python.

doc: [http://jeanphix.me/Ghost.py/](http://jeanphix.me/Ghost.py/)


# 二.demos

## 2.1 urllib

核心接口

### urllib.urlopen(url[, data[, proxies]])

打开一个由URL表示的newwork object以便于读取. 返回file-like object

注意: except for the info(), getcode() and geturl() methods, these methods have the same interface as for file objects

若第一个参数url**没有**模式符号(scheme identifier),或者scheme identifier是file:，则打开一个local file，但是不报扩通用换行符(universal newlines), 否则将打开一个某server上的socket, 出现异常raise IOError.

返回的file-like object支持方法: read(), readline(), readlines(), fileno(), close(), info(), getcode() and geturl()

### urllib.urlretrieve(url[, filename[, reporthook[, data]]])

复制network boject到本地。

If the url uses the http: scheme identifier, the optional data argument may be given to specify a POST request (normally the request type is GET). The data argument must in standard application/x-www-form-urlencoded format;

### 实例

demo1: urllib.urlopen()

```python
# default
>>> import urllib 
>>> url = 'http://www.51voa.com/'
>>> f =urllib.urlopen(url)
>>> f.getcode()
200
>>> f.readline()
'2000\r\n'
>>> f.info()
<httplib.HTTPMessage instance at 0xb7437c8c>
>>> f.geturl()
'http://www.51voa.com/'
```

# 明确指明不使用代理服务器

```python
proxies = {'http':'http://127.0.0.1:8086/'} 
>>> r = urllib.urlopen(url, proxies=None)           #选择不使用代理服务器打开
>>> print r.info() 
Content-Type: text/html; charset=ISO-8859-1
Server: WEBrick/1.3.1 (Ruby/1.9.3/2013-11-22)
Date: Thu, 13 Feb 2014 08:08:47 GMT
Content-Length: 291
Connection: close
```

# 使用代理

```python
>>> proxies = {'http':'http://127.0.0.1:8087'}  
>>> url = 'http://www.python.org'  
>>> r = urllib.urlopen(url, proxies=proxies)      #选择使用代理服务器打开  
  
>>> print r.info()  
>>> print r.getcode()  
>>> print r.geturl()  
  
Content-Length: 21061  
Via: HTTP/1.1 GWA       #通过代理  
Accept-Ranges: bytes  
Vary: Accept-Encoding  
Server: Apache/2.2.16 (Debian)  
Last-Modified: Sat, 06 Jul 2013 21:01:42 GMT  
Etag: "105800d-5245-4e0de1e445980"  
Date: Sun, 07 Jul 2013 06:35:57 GMT  
Content-Type: text/html  
  
200  
http://www.python.org   
```

demo2: urllib.urlretrieve()

# 打开网页

```python
>>> import urllib
>>> url = 'http://www.sina.com.cn/'
>>> req = urllib.urlretrieve(url, '/home/luciuz/sina-weibo.html')
```

# 文件下载1

```
>>> import urllib
>>> url = 'http://211.99.142.59/1Q2W3E4R5T6Y7U8I9O0P1Z2X3C4V5B/down.51voa.com/201402/se-health-mental-training-12feb14.mp3'
>>> urllib.urlretrieve(url, '/tmp/foo1.mp3')
('/tmp/foo1.mp3', <httplib.HTTPMessage instance at 0xb748fc2c>)
>>> from os.path import getsize
>>> getsize('/tmp/foo1.mp3')
3885007L
```

# 文件下载2

```python

#!/usr/bin/env python
# coding=utf-8
import urllib  
  
# 显示进度
def get_progress(a,b,c):  
    '''''回调函数 
    a: 已经下载的数据块 
    b: 数据块的大小 
    c: 远程文件的大小 
    '''  
    process = 100.0*a*b/c  
    if process > 100:  
        process = 100  
    print '%.2f%%' %process   

url = 'http://211.99.142.59/1Q2W3E4R5T6Y7U8I9O0P1Z2X3C4V5B/down.51voa.com/201402/se-health-mental-training-12feb14.mp3'
urllib.urlretrieve(url, '/tmp/foo1.mp3',get_progress)

```

urllib.urlretrieve(url[, filename[, reporthook[, data]]])
