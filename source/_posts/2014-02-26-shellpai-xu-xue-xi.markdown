---
layout: post
title: "Shell排序总结"
date: 2014-02-26 00:49:20 +0800
comments: true
categories: [algorithms]
---

## 分析

以一个相似的假设来理解希尔排序。假设有如下一组数字：

```
～             ～             ～             ～
13 14 94 33 82 25 59 94 65 23 45 27 73 25 39 10
```
### 第一步，以步长4来分组:

```
 A    B    C    D     E
|13 | 14 | 94 | 33  |82 |
|25 | 59 | 94 | 65  |23 |
|45 | 27 | 73 | 25  |39 |
|10 | 
```
对上面各组分别排序:

```
  A   B    C    D    E
|10 | 14 | 73 | 25 | 23 |
|13 | 27 | 94 | 33 | 39 |
|25 | 59 | 94 | 65 | 82 |
|45
```

之后合并:

```
10  14  73  25  23  13  27  94  33  39  25  59  94  65  82  45
```

### 第二步，以步长2分组:

```
~           ~           ~           ~           ~           ~
10  14  73  25  23  13  27  94  33  39  25  59  94  65  82  45
```
分组后:

```
 A    B    C
|10 | 14 | 73 |  
|25 | 23 | 13 |
|27 | 94 | 33 |
|39 | 25 | 59 |
|94 | 65 | 82 |
|45
```
各组分别排序后:

```
 A    B    C
|10 |14 |13 |
|25 |23 |33 |
|27 |25 |59 |
|39 |65 |73 |
|45 |94 |82 |
|94
```
### 第三步，以步长1分组，略；

说明:

真实的Shell排序并非如上所述，而是原址排序。

##  代码

```c
#include <stdio.h>
#include <stdlib.h>


void ShellSort(int a[], int n)
{
    for (int step = n / 2; step >= 1; step /= 2)
    {
        for (int i = step; i < n; i++)
        {
            int key = a[i];
            int j;
            for (j = i; j >= step; j -= step)
            {
                //printf(" key: %d, a[j-step]: %d\n", key, a[j-step]);
                if (key < a[j-step])
                {
                    a[j] = a[j-step];
                }
                else
                    break;
            }
            a[j] = key;
        }
    }
}

int main()
{
    int a[] = {9,32,12,43,11,93,32,43,98,32,12,6,32,91,40,22};
    int size = sizeof(a) / sizeof(int);
    ShellSort(a,size);
    
    for (int i = 0; i < size; i++)
        printf("%d, ", a[i]);
    printf("\n");
}
```
## 复杂度分析

步长为希尔增量(`step1 = [n/2], step2 = [step1/2], ...`), 最坏运行时间为cita(n^2)

Hibbard增量时...
