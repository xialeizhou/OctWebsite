---
layout: post
title: "Linux shell用法总结"
date: 2014-01-16 13:53:46 +0800
comments: true
categories: [linux] 
---
目录：

* Linux 下主要命令总结

* Unix下的20 个习惯

* Linux 命令行省时技巧

-----------------------------

# Part1. Linux 下主要命令总结

使用Linux shell是我每天的基本工作，但我经常会忘记一些有用的shell命令和l技巧。当然，命令我能记住，但我不敢说能记得如何用它执行某个特定任务。于是，我开始在一个文本文件里记录这些用法，并放在我的Dropbox里，现在，向大家分享这个文件。这个文件我会不断的更新。需要注意一点的是，有些用法需要在你的Linux系统里安装额外的软件。

检查远程端口是否对bash开放：

```bash
echo >/dev/tcp/8.8.8.8/53 && echo "open"
```
让进程转入后台：

```bash
Ctrl + z
```

将进程转到前台：

```bash
fg
```

产生随机的十六进制数，其中n是字符数：

```bash
openssl rand -hex n
```

在当前shell里执行一个文件里的命令：

```bash
source /home/user/file.name
```
截取前5个字符：

```bash
${variable:0:5}
```

SSH debug 模式：

```bash
ssh -vvv user@ip_address
```
SSH with pem key：

```bash
ssh user@ip_address -i key.pem
```
用wget抓取完整的网站目录结构，存放到本地目录中：

```bash
wget -r --no-parent --reject "index.html*" http://hostname/ -P /home/user/dirs
```
一次创建多个目录：

```bash
mkdir -p /home/user/{test,test1,test2}
```

列出包括子进程的进程树：

```bash
ps axwef
```

创建 war 文件：

```bash
jar -cvf name.war file
```
  
测试硬盘写入速度：

```bash
dd if=/dev/zero of=/tmp/output.img bs=8k count=256k; rm -rf /tmp/output.img
```

测试硬盘读取速度：

```bash
hdparm -Tt /dev/sda
```

获取文本的md5 hash：

```bash
echo -n "text" | md5sum
```

检查xml格式：

```bash
xmllint --noout file.xml
```

将tar.gz提取到新目录里：

```bash
tar zxvf package.tar.gz -C new_dir
```

使用curl获取HTTP头信息：

```bash
curl -I http://www.example.com
```

修改文件或目录的时间戳(YYMMDDhhmm)：

```bash
touch -t 0712250000 file
```

用wget命令执行ftp下载：

```bash
wget -m ftp://username:password@hostname
```

生成随机密码(例子里是16个字符长)：

```bash
LANG=c < /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-16};echo;
```

快速备份一个文件：

```bash
cp some_file_name{,.bkp}
```

访问Windows共享目录：

```bash
smbclient -U "DOMAIN\user" //dc.domain.com/share/test/dir
```

执行历史记录里的命令(这里是第100行)：

```bash
!100
```

解压：

```bash
unzip package_name.zip -d dir_name
```

输入多行文字(CTRL + d 退出)：

```bash
cat > test.txt
```

创建空文件或清空一个现有文件：

```bash
> test.txt
```

与Ubuntu NTP server同步时间：

```bash
ntpdate ntp.ubuntu.com
```

用netstat显示所有tcp4监听端口：

```bash
netstat -lnt4 | awk '{print $4}' | cut -f2 -d: | grep -o '[0-9]*'
```
qcow2镜像文件转换：

```bash
qemu-img convert -f qcow2 -O raw precise-server-cloudimg-amd64-disk1.img \
                                 precise-server-cloudimg-amd64-disk1.raw
```

重复运行文件，显示其输出（缺省是2秒一次）：

```bash
watch ps -ef
```

所有用户列表：

```bash
getent passwd
```

Mount root in read/write mode:

```bash
mount -o remount,rw /
```

挂载一个目录（这是不能使用链接的情况）：

```bash
mount --bind /source /destination
```

动态更新DNS server:

```bash
nsupdate < <EOF
update add $HOST 86400 A $IP
send
EOF
```
递归grep所有目录：

```bash
grep -r "some_text" /path/to/dir
```
列出前10个最大的文件：

```bash  
lsof / | awk '{ if($7 > 1048576) print $7/1048576 "MB "$9 }' | sort -n -u | tail
```

显示剩余内存(MB)：

```bash
free -m | grep cache | awk '/[0-9]/{ print $4" MB" }'
```
打开Vim并跳到文件末：

```bash
vim + some_file_name
```

Git 克隆指定分支(master)：

```bash
git clone git@github.com:name/app.git -b master
```
Git 切换到其它分支(develop)：

```bash
git checkout develop
```
Git 删除分支(myfeature)：

```bash
git branch -d myfeature
```
Git 删除远程分支：

```bash
git push origin :branchName
```

Git 将新分支推送到远程服务器：

```bash
git push -u origin mynewfeature
```
打印历史记录中最后一次cat命令：

```bash
!cat:p
```
运行历史记录里最后一次cat命令：

```bash
!cat
```

找出/home/user下所有空子目录：

```bash
find /home/user -maxdepth 1 -type d -empty
```
获取test.txt文件中第50-60行内容：

```bash
< test.txt sed -n '50,60p'
```

运行最后一个命令(如果最后一个命令是mkdir /root/test, 下面将会运行: sudo mkdir /root/test)：

```bash
sudo !!
```

创建临时RAM文件系统 – ramdisk (先创建/tmpram目录)：
 
```bash
mount -t tmpfs tmpfs /tmpram -o size=512m
```
Grep whole words:

```bash
grep -w "name" test.txt
```

在需要提升权限的情况下往一个文件里追加文本：

```bash
echo "some text" | sudo tee -a /path/file
```

列出所有kill signal参数：

```bash
kill -l
```

在bash历史记录里禁止记录最后一次会话：

```bash
kill -9 $$
```

扫描网络寻找开放的端口：

```bash  
nmap -p 8081 172.20.0.0/16
```

设置git email:
  
```bash
git config --global user.email "me@example.com"
```
To sync with master if you have unpublished commits:
  
```bash
git pull --rebase origin master
```
将所有文件名中含有”txt”的文件移入/home/user目录：

```bash
find -iname "*txt*" -exec mv -v {} /home/user \;
```

将文件按行并列显示：
  
```bash
paste test.txt test1.txt
```

shell里的进度条：
  
```bash
pv data.log
```

使用netcat将数据发送到Graphite server:

```bash
echo "hosts.sampleHost 10 `date +%s`" | nc 192.168.200.2 3000
```

将tabs转换成空格：
  
```bash
expand test.txt > test1.txt
```

Skip bash history:
  
```bash
< space >cmd
```

去之前的工作目录：
  
```bash
cd -
```

拆分大体积的tar.gz文件(每个100MB)，然后合并回去：
  
```bash
split –b 100m /path/to/large/archive /path/to/output/files
cat files* > archive
```

使用curl获取HTTP status code:
  
```bash
curl -sL -w "%{http_code}\\n" www.example.com -o /dev/null
```

设置root密码，强化MySQL安全安装：
  
```bash
/usr/bin/mysql_secure_installation
```

当Ctrl + c不好使时：
  
```bash
Ctrl + \
```

获取文件owner:
  
```bash
stat -c %U file.txt
```

block设备列表：
  
```bash
lsblk -f
```

找出文件名结尾有空格的文件：
  
```bash
find . -type f -exec egrep -l " +$" {} \;
```

找出文件名有tab缩进符的文件：
  
```bash
find . -type f -exec egrep -l $'\t' {} \;
```

用”=”打印出横线：
  
```bash
printf '%100s\n' | tr ' ' =
```

# Part2. Unix下的20 个习惯

采用 10 个能够提高您的 UNIX 命令行效率的好习惯——并在此过程中摆脱不良的使用模式。本文循序渐进地指导您学习几项用于命令行操作的技术，这些技术非常好，但是通常被忽略。了解常见错误和克服它们的方法，以便您能够确切了解为何值得采用这些 UNIX 习惯。

## 引言

当您经常使用某个系统时，往往会陷入某种固定的使用模式。有时，您没有养成以尽可能最好的方式做事的习惯。有时，您的不良习惯甚至会导致出现混乱。纠正此类缺点的最佳方法之一，就是有意识地采用抵制这些坏习惯的好习惯。本文提出了 10 个值得采用的 UNIX 命令行习惯——帮助您克服许多常见使用怪癖，并在该过程中提高命令行工作效率的好习惯。下面列出了这 10 个好习惯，之后对其进行了更详细的描述。

1.  在单个命令中创建目录树。
2.  更改路径；不要移动存档。
3.  将命令与控制操作符组合使用。
4.  谨慎引用变量。
5.  使用转义序列来管理较长的输入。
6.  在列表中对命令分组。
7.  在 find 之外使用 xargs。
8.  了解何时 grep 应该执行计数——何时应该绕过。
9.  匹配输出中的某些字段，而不只是对行进行匹配。
10. 停止对 cat 使用管道
11. 使用文件名自动完成功能 (file name completion)。
12. 使用历史扩展。
13. 重用以前的参数。
14. 使用 pushd 和 popd 管理目录导航。
15. 查找大型文件。
16. 不使用编辑器创建临时文件。
17. 使用 curl 命令行实用工具。
18. 最有效地利用正则表达式。
19. 确定当前用户。
20. 使用 awk 处理数据。

## 在单个命令中创建目录树

清单 1 演示了最常见的 UNIX 坏习惯之一：一次定义一个目录树。
清单 1. 坏习惯 1 的示例：单独定义每个目录树

```bash
~ $ mkdir tmp
~ $ cd tmp
~/tmp $ mkdir a
~/tmp $ cd a
~/tmp/a $ mkdir b
~/tmp/a $ cd b
~/tmp/a/b/ $ mkdir c
~/tmp/a/b/ $ cd c
~/tmp/a/b/c $
```
使用 mkdir 的 -p 选项并在单个命令中创建所有父目录及其子目录要容易得多。但是即使对于知道此选项的管理员，他们在命令行上创建子目录时也仍然束缚于逐步创建每级子目录。花时间有意识地养成这个好习惯是值得的：

清单 2. 好习惯 1 的示例：使用一个命令来定义目录树

```bash
~ $ mkdir -p tmp/a/b/c
```
您可以使用此选项来创建整个复杂的目录树（在脚本中使用是非常理想的），而不只是创建简单的层次结构。例如：

清单 3. 好习惯 1 的另一个示例：使用一个命令来定义复杂的目录树

```bash
~ $ mkdir -p project/{lib/ext,bin,src,doc/{html,info,pdf},demo/stat/a}
```

过去，单独定义目录的唯一借口是您的 mkdir 实现不支持此选项，但是在大多数系统上不再是这样了。IBM、AIX 、mkdir、GNU mkdir 和其他遵守单一 UNIX 规范 (Single UNIX Specification) 的系统现在都具有此选项。

对于仍然缺乏该功能的少数系统，您可以使用 mkdirhier 脚本，此脚本是执行相同功能的 mkdir 的包装：

```bash
~ $ mkdirhier project/{lib/ext,bin,src,doc/{html,info,pdf},demo/stat/a}
```

## 更改路径；不要移动存档

另一个不良的使用模式是将 .tar 存档文件移动到某个目录，因为该目录恰好是您希望在其中提取 .tar 文件的目录。其实您根本不需要这样做。您可以随心所欲地将任何 .tar 存档文件解压缩到任何目录——这就是 -C 选项的用途。在解压缩某个存档文件时，使用 -C 选项来指定要在其中解压缩该文件的目录：

清单 4. 好习惯 2 的示例：使用选项 -C 来解压缩 .tar 存档文件

```bash
~ $ tar xvf -C tmp/a/b/c newarc.tar.gz
```

相对于将存档文件移动到您希望在其中解压缩它的位置，切换到该目录，然后才解压缩它，养成使用 -C 的习惯则更加可取——当存档文件位于其他某个位置时尤其如此。

## 将命令与控制操作符组合使用

您可能已经知道，在大多数 Shell 中，您可以在单个命令行上通过在命令之间放置一个分号 (;) 来组合命令。该分号是 Shell 控制操作符，虽然它对于在单个命令行上将离散的命令串联起来很有用，但它并不适用于所有情况。例如，假设您使用分号来组合两个命令，其中第二个命令的正确执行完全依赖于第一个命令的成功完成。如果第一个命令未按您预期的那样退出，第二个命令仍然会运行——结果会导致失败。相反，应该使用更适当的控制操作符（本文将描述其中的部分操作符）。只要您的 Shell 支持它们，就值得养成使用它们的习惯。

## 仅当另一个命令返回零退出状态时才运行某个命令

使用 && 控制操作符来组合两个命令，以便仅当 第一个命令返回零退出状态时才运行第二个命令。换句话说，如果第一个命令运行成功，则第二个命令将运行。如果第一个命令失败，则第二个命令根本就不运行。例如：

清单 5. 好习惯 3 的示例：将命令与控制操作符组合使用
  
```bash
~ $ cd tmp/a/b/c && tar xvf ~/archive.tar
```

在此例中，存档的内容将提取到 ~/tmp/a/b/c 目录中，除非该目录不存在。如果该目录不存在，则 tar 命令不会运行，因此不会提取任何内容。

## 仅当另一个命令返回非零退出状态时才运行某个命令

类似地，|| 控制操作符分隔两个命令，并且仅当第一个命令返回非零退出状态时才运行第二个命令。换句话说，如果第一个命令成功，则第二个命令不会运行。如果第一个命令失败，则第二个命令才会 运行。在测试某个给定目录是否存在时，通常使用此操作符，如果该目录不存在，则创建它：

清单 6. 好习惯 3 的另一个示例：将命令与控制操作符组合使用

```bash
~ $ cd tmp/a/b/c || mkdir -p tmp/a/b/c
```

您还可以组合使用本部分中描述的控制操作符。每个操作符都影响最后的命令运行：

清单 7. 好习惯 3 的组合示例：将命令与控制操作符组合使用

```bash
~ $ cd tmp/a/b/c || mkdir -p tmp/a/b/c && tar xvf -C tmp/a/b/c ~/archive.tar
```

## 谨慎引用变量

始终要谨慎使用 Shell 扩展和变量名称。一般最好将变量调用包括在双引号中，除非您有不这样做的足够理由。类似地，如果您直接在字母数字文本后面使用变量名称，则还要确保将该变量名称包括在方括号 ([]) 中，以使其与周围的文本区分开来。否则，Shell 将把尾随文本解释为变量名称的一部分——并且很可能返回一个空值。清单 8 提供了变量的各种引用和非引用及其影响的示例。

清单 8. 好习惯 4 的示例：引用（和非引用）变量

```bash
~ $ ls tmp/
a b
~ $ VAR="tmp/*"
~ $ echo $VAR
tmp/a tmp/b
~ $ echo "$VAR"
tmp/*
~ $ echo $VARa
 
~ $ echo "$VARa"
 
~ $ echo "${VAR}a"
tmp/*a
~ $ echo ${VAR}a
tmp/a
~ $
```

## 使用转义序列来管理较长的输入

您或许看到过使用反斜杠 (\) 来将较长的行延续到下一行的代码示例，并且您知道大多数 Shell 都将您通过反斜杠联接的后续行上键入的内容视为单个长行。然而，您可能没有在命令行中像通常那样利用此功能。如果您的终端无法正确处理多行回绕，或者您的命令行比通常小（例如在提示符下有长路经的时候），反斜杠就特别有用。反斜杠对于了解键入的长输入行的含义也非常有用，如以下示例所示：

清单 9. 好习惯 5 的示例：将反斜杠用于长输入

```bash
~ $ cd tmp/a/b/c || \
> mkdir -p tmp/a/b/c && \
> tar xvf -C tmp/a/b/c ~/archive.tar
```

或者，也可以使用以下配置：
清单 10. 好习惯 5 的替代示例：将反斜杠用于长输入

```bash
~ $ cd tmp/a/b/c \
>                 || \
> mkdir -p tmp/a/b/c \
>                    && \
> tar xvf -C tmp/a/b/c ~/archive.tar
```

然而，当您将输入行划分到多行上时，Shell 始终将其视为单个连续的行，因为它总是删除所有反斜杠和额外的空格。

注意：在大多数 Shell 中，当您按向上箭头键时，整个多行输入将重绘到单个长输入行上。

## 在列表中对命令分组

大多数 Shell 都具有在列表中对命令分组的方法，以便您能将它们的合计输出向下传递到某个管道，或者将其任何部分或全部流重定向到相同的地方。您一般可以通过在某个 Subshell 中运行一个命令列表或通过在当前 Shell 中运行一个命令列表来实现此目的。

## 在 Subshell 中运行命令列表

使用括号将命令列表包括在单个组中。这样做将在一个新的 Subshell 中运行命令，并允许您重定向或收集整组命令的输出，如以下示例所示：

清单 11. 好习惯 6 的示例：在 Subshell 中运行命令列表

```bash
~ $ ( cd tmp/a/b/c/ || mkdir -p tmp/a/b/c && \
> VAR=$PWD; cd ~; tar xvf -C $VAR archive.tar ) \
> | mailx admin -S "Archive contents"
```

在此示例中，该存档的内容将提取到 tmp/a/b/c/ 目录中，同时将分组命令的输出（包括所提取文件的列表）通过邮件发送到地址 admin。

当您在命令列表中重新定义环境变量，并且您不希望将那些定义应用于当前 Shell 时，使用 Subshell 更可取。

## 在当前 Shell 中运行命令列表

将命令列表用大括号 ({}) 括起来，以在当前 Shell 中运行。确保在括号与实际命令之间包括空格，否则 Shell 可能无法正确解释括号。此外，还要确保列表中的最后一个命令以分号结尾，如以下示例所示：
清单 12. 好习惯 6 的另一个示例：在当前 Shell 中运行命令列表

```bash
~ $ { cp ${VAR}a . && chown -R guest.guest a && \
> tar cvf newarchive.tar a; } | mailx admin -S "New archive"
```

## 在 find 之外使用 xargs

使用 xargs 工具作为筛选器，以充分利用从 find 命令挑选的输出。find 运行通常提供与某些条件匹配的文件列表。此列表被传递到 xargs上，后者然后使用该文件列表作为参数来运行其他某些有用的命令，如以下示例所示：

清单 13. xargs 工具的经典用法示例

```bash
~ $ find some-file-criteria some-file-path | \
> xargs some-great-command-that-needs-filename-arguments
```

然而，不要将 xargs 仅看作是 find 的辅助工具；它是一个未得到充分利用的工具之一，当您养成使用它的习惯时，将会希望进行所有试验，包括以下用法。

## 传递空格分隔的列表

在最简单的调用形式中，xargs 就像一个筛选器，它接受一个列表（每个成员分别在单独的行上）作为输入。该工具将那些成员放置在单个空格分隔的行上：

清单 14. xargs 工具产生的输出示例

```bash
~ $ xargsabcControl-D
a b c
~ $
```

您可以发送通过 xargs 来输出文件名的任何工具的输出，以便为其他某些接受文件名作为参数的工具获得参数列表，如以下示例所示：
清单 

15. xargs 工具的使用示例

```bash
~/tmp $ ls -1 | xargs
December_Report.pdf README a archive.tar mkdirhier.sh
~/tmp $ ls -1 | xargs file
December_Report.pdf: PDF document, version 1.3
README: ASCII text
a: directory
archive.tar: POSIX tar archive
mkdirhier.sh: Bourne shell script text executable
~/tmp $
```
xargs 命令不只用于传递文件名。您还可以在需要将文本筛选到单个行中的任何时候使用它：

清单 16. 好习惯 7 的示例：使用 xargs 工具来将文本筛选到单个行中

```bash
~/tmp $ ls -l | xargs
-rw-r--r-- 7 joe joe 12043 Jan 27 20:36 December_Report.pdf -rw-r--r-- 1 \
root root 238 Dec 03 08:19 README drwxr-xr-x 38 joe joe 354082 Nov 02 \
16:07 a -rw-r--r-- 3 joe joe 5096 Dec 14 14:26 archive.tar -rwxr-xr-x 1 \
joe joe 3239 Sep 30 12:40 mkdirhier.sh
~/tmp $
```

## 谨慎使用 xargs

从技术上讲，使用 xargs 很少遇到麻烦。缺省情况下，文件结束字符串是下划线 (_)；如果将该字符作为单个输入参数来发送，则它之后的所有内容将被忽略。为了防止这种情况发生，可以使用 -e 标志，它在不带参数的情况下完全禁用结束字符串。

## 了解何时 grep 应该执行计数——何时应该绕过

避免通过管道将 grep 发送到 wc -l 来对输出行数计数。grep 的 -c 选项提供了对与特定模式匹配的行的计数，并且一般要比通过管道发送到wc 更快，如以下示例所示：

清单 17. 好习惯 8 的示例：使用和不使用 grep 的行计数

```bash
~ $ time grep and tmp/a/longfile.txt | wc -l
2811
 
real    0m0.097s
user    0m0.006s
sys     0m0.032s
~ $ time grep -c and tmp/a/longfile.txt
2811
 
real    0m0.013s
user    0m0.006s
sys     0m0.005s
~ $
```

除了速度因素外，-c 选项还是执行计数的好方法。对于多个文件，带 -c 选项的 grep 返回每个文件的单独计数，每行一个计数，而针对 wc 的管道则提供所有文件的组合总计数。

然而，不管是否考虑速度，此示例都表明了另一个要避免地常见错误。这些计数方法仅提供包含匹配模式的行数——如果那就是您要查找的结果，这没什么问题。但是在行中具有某个特定模式的多个实例的情况下，这些方法无法为您提供实际匹配实例数量 的真实计数。归根结底，若要对实例计数，您还是要使用 wc 来计数。首先，使用 -o 选项（如果您的版本支持它的话）来运行 grep 命令。此选项仅 输出匹配的模式，每行一个模式，而不输出行本身。但是您不能将它与 -c 选项结合使用，因此要使用 wc -l 来对行计数，如以下示例所示：

清单 18. 好习惯 8 的示例：使用 grep 对模式实例计数

```bash
~ $ grep -o and tmp/a/longfile.txt | wc -l
3402
~ $
```

在此例中，调用 wc 要比第二次调用 grep 并插入一个虚拟模式（例如 grep -c）来对行进行匹配和计数稍快一点

## 匹配输出中的某些字段，而不只是对行进行匹配

当您只希望匹配输出行中特定字段 中的模式时，诸如 awk 等工具要优于 grep。

下面经过简化的示例演示了如何仅列出 12 月修改过的文件。

清单 19. 坏习惯 9 的示例：使用 grep 来查找特定字段中的模式

```bash
~/tmp $ ls -l /tmp/a/b/c | grep Dec
-rw-r--r--  7 joe joe  12043 Jan 27 20:36 December_Report.pdf
-rw-r--r--  1 root root  238 Dec 03 08:19 README
-rw-r--r--  3 joe joe   5096 Dec 14 14:26 archive.tar
~/tmp $

```

在此示例中，grep 对行进行筛选，并输出其修改日期和名称中带 Dec 的所有文件。因此，诸如 December_Report.pdf 等文件是匹配的，即使它自从一月份以来还未修改过。这可能不是您希望的结果。为了匹配特定字段中的模式，最好使用 awk，其中的一个关系运算符对确切的字段进行匹配，如以下示例所示：

清单 20. 好习惯 9 的示例：使用 awk 来查找特定字段中的模式

```bash
~/tmp $ ls -l | awk '$6 == "Dec"'
-rw-r--r--  3 joe joe   5096 Dec 14 14:26 archive.tar
-rw-r--r--  1 root root  238 Dec 03 08:19 README
~/tmp $
```

## 停止对 cat 使用管道

grep 的一个常见的基本用法错误是通过管道将 cat 的输出发送到 grep 以搜索单个文件的内容。这绝对是不必要的，纯粹是浪费时间，因为诸如 grep 这样的工具接受文件名作为参数。您根本不需要在这种情况下使用 cat，如以下示例所示：

清单 21. 好习惯和坏习惯 10 的示例：使用带和不带 cat 的 grep

```bash
~ $ time cat tmp/a/longfile.txt | grep and
2811
 
real    0m0.015s
user    0m0.003s
sys     0m0.013s
~ $ time grep and tmp/a/longfile.txt
2811
 
real    0m0.010s
user    0m0.006s
sys     0m0.004s
~ $
```

此错误存在于许多工具中。由于大多数工具都接受使用连字符 (-) 的标准输入作为一个参数，因此即使使用 cat 来分散 stdin 中的多个文件，参数也通常是无效的。仅当您使用带多个筛选选项之一的 cat 时，才真正有必要在管道前首先执行连接。

## 常用首字母缩写词

*   MB：兆字节
*   HTTP：超文本传输协议
*   HTTPS：HTTP over Secure Sockets Layer
*   FTP：文件传输协议
*   FTPS：FTP over Secure Sockets Layer
*   LDAP：轻型目录访问协议

## 使用文件名完成

如果不需要在命令提示符处键入长的、令人费解的文件名，这是不是很棒呢？的确，您不需要这样做。相反，您可以配置最流行的 UNIX Shell 以使用文件名完成。该功能在各个 Shell 中的工作方式略有不同，因此我将向您展示如何在最流行的 Shell 中使用文件名完成。文件名完成使您可以更快地输入并避免错误。懒惰？也许吧。效率更高？当然！

## 我正在运行哪种 Shell？

如果您不知道目前使用的是哪一种 Shell，会怎么样？虽然这个诀窍不是另外 10 个好习惯的正式组成部分，但它仍然很有用。如清单 1 所示，您可以使用 echo $0 或 ps -p $$ 命令显示您正在使用的 Shell。对于我来说，运行的是 Bash Shell。

清单 1. 确定您的 Shell

```bash
$ echo $0
-bash
$ ps –p $$
PID TTY           TIME CMD
6344 ttys000    0:00.02 –bash
```

**C Shell**

C Shell 支持最直接文件名完成功能。设置 filec 变量可启用该功能。（您可以使用命令 set filec。）在您开始键入文件名后，可以按 Esc键，Shell 将完成文件名，或完成尽可能多的部分。例如，假设您拥有名为 file1、file2 和 file3 的文件。如果您键入 f，然后按 Esc 键，将填充file，而您必须键入 1、2 或 3 来完成相应的文件名。

**Bash**

Bash Shell 也提供了文件名完成，但使用 Tab 键代替 Esc 键。您在 Bash Shell 中不需要设置任何选项即可启用文件名完成，该选项是缺省设置的。Bash 还实现了其他功能。键入文件名的一部分后，按 Tab 键，如果有多个文件满足您的请求，并且您需要添加文本以选择其中一个文件，那么您可以多按 Tab 键两次，以显示与您目前键入的内容相匹配的文件的列表。使用之前名为 file1、file2 和 file3 的文件示例，首先键入f。当您按一次 Tab 键时，Bash 完成 file；再按一次 Tab 键时，将展开列表 file1 file2 file3。

**Korn Shell*

对于 Korn Shell 用户，文件名完成取决于 EDITOR 变量的值。如果 EDITOR 设置为 vi，那么您键入部分名称，然后按 Esc 键，后跟反斜杠 (\) 字符。如果 EDITOR 设置为 emacs，那么您键入部分名称，然后按两次 Esc 键以完成文件名。

## 使用历史扩展

如果您为一系列命令使用相同的文件名，会发生什么情况？当然，有一种快捷方式可以快速获得您上次使用的文件名。如清单 2 所示，!$ 命令返回前一个命令使用的文件名。从文件 this-is-a-long-lunch-menu-file.txt 中搜索单词 pickles 的出现位置。搜索结束后，使用 vi 命令来编辑 this-is-a-long-lunch-menu-file.txt 文件，而不需要重新键入文件名。您使用感叹号 (!) 来访问历史，然后使用美元符号 ($) 返回前一命令的最后字段。如果您反复用到长文件名，那么这是一个非常好的工具。

清单 2. 使用 !$ 获得前一个命令使用的文件名

```bash
$ grep pickles this-is-a-long-lunch-menu-file.txt
pastrami on rye with pickles and onions
$ vi !$
```

## 重用以前的参数
