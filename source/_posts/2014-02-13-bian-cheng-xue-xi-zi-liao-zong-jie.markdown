---
layout: post
title: "编程学习资料总结"
date: 2014-02-13 01:45:58 +0800
comments: true
categories: [study]
---

基本涵盖python, hadoop, 数据结构与算法，github, c语言以及java

## 1. python

### 1.1   [Python Documentation](http://www.python.org/doc/)

* [The Python Tutorial](http://docs.python.org/2/tutorial/)

  python 基础学习资料，整体了解
  
* [The Python Standard Library](http://docs.python.org/2/library/)

  Built-in Types, String Services, Data Types, File and Directory Access, Generic Operating System Services...

* [The Python Language Reference](http://docs.python.org/2/reference/)
  
  语法详细介绍，核心语义(core semantics), 好象是从开发者的角度深入剖析python的实现细节

###   1.2 [PerlPhrasebook](https://wiki.python.org/moin/PerlPhrasebook#Reading_from_a_file_line_by_line)
  
   比较perl和python的异同

###   1.3 [Python HOWTOs](http://docs.python.org/2/howto/index.html#python-howtos)
  
*    Porting Python 2 Code to Python 3
*    Porting Extension Modules to Python 3
*    Curses Programming with Python
*    Descriptor HowTo Guide
*    Idioms and Anti-Idioms in Python
*    Functional Programming HOWTO
*    Logging HOWTO
*    Logging Cookbook
*    Regular Expression HOWTO
*    Socket Programming HOWTO
*    Sorting HOW TO
*    Unicode HOWTO
*    HOWTO Fetch Internet Resources Using urllib2
*    HOWTO Use Python in the web
*    Argparse Tutorial

## 2. c language
