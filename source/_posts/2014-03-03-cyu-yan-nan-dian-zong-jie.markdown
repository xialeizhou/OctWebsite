---
layout: post
title: "C语言重要知识点总结"
date: 2014-03-03 20:20:00 +0800
comments: true
categories: [c]
---

## static 关键字

### 三类： static全局变量，static局部变量，static函数

### 作用: 

**只具有文件作用域**   限制全局变量和函数只具有文件作用域 

**记忆功能**  使局部变量具有记忆功能，其值一直保存在内存中，下次调用维持改变过的值

**只初始化一次**  static类型的变量只初始化一次，反复调用多次的定义将会被忽略

### 示例

####  1.只具有文件作用域 

**demo1** -- 全局静态变量

```c  ex1.c
#include <stdio.h>

int main()
{
  extern int val;
  printf("val is %d\n", val);
}
```

```c  ex2.c
int val = 999; // static int val = 999;
```

反编译后:

```c  ex2.s
.globl val //只有添加static关键字后才有
  .data
  .align 4
  .type val, @object
  .size val, 4
val:
  .long 999
```

**demo2** -- static函数

```c  ex1.c
#include <stdio.h>

int call() // static int call()
{
  printf("this is ex2!\n");
}
```

```c  ex2.c
int call();

int main() 
{
  call();
}

```
反编译后核心代码:

```c
.globl call             // 添加static关键字后，没有这一句
  .type call, @function
  
call:
  pushl %ebp
  movl  %esp, %ebp
  subl  $24, %esp
  movl  $.LC0, (%esp)
  call  puts
  leave
```

####  2.记忆功能 | 只初始化一次

以上两点可由下面的demo体现:

**demo3** -- 记忆功能导致多次重复定义某一静态局部变量将被忽略

```c
#include <stdio.h>

void fun()
{
  static int i = 8;
  i++;
  printf("i: %d\n", i);
}

int main()
{
  fun();
  fun();
  fun();
}
```
输出: 

```bash
i: 9
i: 10
i: 11
```
若为普通局部变量, 输出:

```c
i: 9
i: 9
i: 9
```

反编译后的不同:

```c
.data                    //若为普通局部变量，则没有这几段代码
 .align 4
 .type i.1453, @object  //从这几段汇编码可以看出，添加static关键字后的局部变量，其值保存在内存区，
 .size i.1453, 4        //而不是像普通局部变量一样，保存在栈区
i.1453:
  .long 8
```

## const关键字

除了保护变量和函数参数值不被意外修改外，比较典型的应用有以下几点:

节省空间，避免不必要的内存分配(重要, 内存分配不是在定义时，而在其它变量被赋给此值时)

修饰常引用(重要)

修饰类的成员函数(重要)

### 1.节省空间

demo1

```c
const double Pi = 3.14; 
double i = Pi; 
double j = Pi;
```

下面是汇编码:

情形1 -- Pi是局部变量时

```c
main:
  pushl %ebp
  movl  %esp, %ebp
  subl  $16, %esp
  movl  $999, -12(%ebp) //将const常量以立即数的形式压入栈中
  movl  -12(%ebp), %eax //间接寻址，将999赋给i
  movl  %eax, -8(%ebp)
  movl  -12(%ebp), %eax //间接寻址，将999赋给j
  movl  %eax, -4(%ebp)
  movl  $0, %eax
  leave
  ret 
```

特点:

只给Pi, i, j都占用栈空间

情形2 -- Pi是全局变量

```c
.globl val 
  .section  .rodata
  .align 4
  .type val, @object
  .size val, 4
val:
  .long 999  // val是全局常量时，以符号表的形式保存,本身不给其分配内存
  .text
.globl main
  .type main, @function
main:
  pushl %ebp
  movl  %esp, %ebp
  subl  $16, %esp
  movl  val, %eax 
  movl  %eax, -8(%ebp) //给i分配内存
  movl  val, %eax
  movl  %eax, -4(%ebp) //给j分配内存
  movl  $0, %eax
  leave
  ret 
```

特点:

只有一个, 即`const val = 999;`是全局变量时，不给val本身分配内存，故总体而言，起到了节省内存的作用。

### 2.修饰常引用

普通引用不接受类似常数等左值, 常引用可以接受左值, 编译器会自动做中间转换.

demo

```c
double &dr = 1; //错误! 普通引用不接受左值!
const double &dr = 1; //正确!
```

编译器转诨过程:

```c
double temp = double(1);
const double &dr = temp;
```
