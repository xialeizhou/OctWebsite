---
layout: post
title: "归并排序总结"
date: 2014-02-26 00:53:40 +0800
comments: true
categories: [algorithms]
---

## 分析

关键： 

Merge步骤中，常规方法需要考虑两个子数组的边界问题, 若L1先到达边界，则直接将L2剩余部分全部导入目标数组即可。

为了简化代码，可以引入哨兵(Seltinel), 哨兵取值为无穷大或者远大与传入数组的最大值， 将其置于子数组L1和L2末尾。

代码1 -- 带哨兵的Merge()

```c
#include <stdio.h>
#include <stdlib.h>

#define MAX_INTEGER 9999

void Merge(int a[], int p, int q, int r)
{
    int n1 = q - p + 1;
    int n2 = r - q;
    
    int L1[n1 + 1];
    int L2[n2 + 1];
    
    for (int i = 0; i < n1; i++)
        L1[i] = a[p + i];
    for (int j = 0; j < n2; j++)
        L2[j] = a[q + 1 + j];
    
    L1[n1] = MAX_INTEGER;
    L2[n2] = MAX_INTEGER;
    
    int i = 0, j = 0;
    for(int k = p; k <= r; k++)
    {
        if (L1[i] <= L2[j])
            a[k] = L1[i++];
        else
            a[k] = L2[j++];
    }
}
```

代码2 -- 不带哨兵的Merge函数

```c
void Merge2(int a[], int p, int q, int r)
{
  int n1 = q - p + 1;
  int n2 = r - q;
  int L1[n1];
  int L2[n2];

  for(int i = 0; i < n1; i++)
    L1[i] = a[p + i];
  for(int j = 0; j < n2; j++)
    L2[j] = a[q + 1 + j];
  
  int i = 0, j = 0;
  for(int k = p; k <= r; k++)
  {
    if(i == n1)
    {
      while(j < n2 && k <= r)
        a[k++] = L2[j++];
      break;
    }
    else if(j == n2)
    {
      while(i < n1 && k <= r)
        a[k++] = L1[i++];
      break;
    }

    if(L1[i] < L2[j])
      a[k] = L1[i++];
    else
      a[k] = L2[j++];
  }
}
```

代码3 MergeSort()

```c
void MergeSort(int a[], int p, int r)
{
    if ( p < r)
    {
        int q = (p + r) / 2;
        MergeSort(a, p, q);
        MergeSort(a, q+1, r);
        Merge(a, p, q, r); // or Merge2(a, p, q, r);
    }
}
```
