---
layout: post
title: "算法资料总结"
date: 2014-01-18 02:05:03 +0800
comments: true
categories: [algorithms]
---

## 矩阵乘法

* [Strassen’s Matrix Multiplication](http://www.stoimen.com/blog/2012/11/26/computer-algorithms-strassens-matrix-multiplication/)
关键是Strassen自己定义的那七个矩阵

* [Karatsuba Fast Multiplication](http://www.stoimen.com/blog/2012/05/15/computer-algorithms-karatsuba-fast-multiplication/)
