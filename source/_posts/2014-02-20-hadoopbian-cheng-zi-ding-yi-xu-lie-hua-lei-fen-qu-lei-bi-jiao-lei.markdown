---
layout: post
title: "hadoop编程-自定义序列化类|分区类|比较类"
date: 2014-02-20 01:25:13 +0800
comments: true
categories: [hadoop]
---

已经实现的需求：

*    自定义TextIntWritable序列化类型--基本的序列化类型
*
*    自定义比较器--setSortComparatorClass（实现了两个：原生比较器，反序列化后比较器）
*
*    二次排序
*
*    自定义分区类--setPartitionerClass
*
*    自定义分组类--setGroupingComparatorClass

未实现的需求：自定义FileIntputFormat。

测试数据： 

```
    Green:7

    Lucius:5

    Lucy:8

    Jack:9

    Lucius:6

    Green:4

    Jack:10
```
代码如下：

```java
import java.io.IOException;
import java.io.DataOutput;
import java.io.DataInput;
import java.lang.StringBuffer;
           
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.io.WritableUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
           
public class SortByValue extends Configured implements Tool {
           
  // 自定义序列化类型（Text,Int）
  public static class TextIntWritable implements WritableComparable<TextIntWritable> {
    private String thisString;
    private int thatValue;
           
    public TextIntWritable() {}
           
    public TextIntWritable(String str, int val) {
      set(str,val);
    }
           
    public void set(String str, int val) {
      this.thisString = str;
      this.thatValue = val;
    }
           
    public String getFirst() { return this.thisString; }
    public int getSecond() { return this.thatValue; }
           
    @Override
      public void readFields(DataInput in) throws IOException {
        thisString = in.readUTF();
        thatValue = in.readInt();
      }
           
    @Override
      public void write(DataOutput out) throws IOException {
        out.writeUTF(thisString);
        out.writeInt(thatValue);
      }
           
    @Override
      public int compareTo(TextIntWritable o) {
        return  - this.thisString.compareTo(o.thisString);
      }
           
  }
           
  // map输出： ((user1,f1), f1)
  public static class Map extends Mapper<LongWritable, Text, TextIntWritable, IntWritable> {
    @Override
      public void map(LongWritable key, Text val, Context context) 
      throws IOException, InterruptedException {
        String line = val.toString();
        Integer id = Integer.parseInt(line.split(":")[1]);
        String user = line.split(":")[0];
        context.write(new TextIntWritable(user,id), new IntWritable(id));
      }
  }
             
  // 输出结果： (user1 (f1, f2, f3))
  public static class Reduce extends Reducer<TextIntWritable, IntWritable, Text, Text> {
    @Override
      public void reduce(TextIntWritable key, Iterable<IntWritable>vals, Context context) 
      throws IOException, InterruptedException {
        StringBuffer combineValue = new StringBuffer();
        for(IntWritable val : vals) {
          if(combineValue.length() > 0) combineValue.append(",");
          combineValue.append(val);
        }
        context.write(new Text(key.getFirst()), new Text(combineValue.toString()));
      }
  }
           
  // 自定义反序列化后二次排序比较器，map输出的key中两个值都作为比较条件
  public static class TextIntComparator extends WritableComparator {
           
    public TextIntComparator(){
      super(TextIntWritable.class, true);
    }
           
    @SuppressWarnings("rawtypes")
      @Override
      public int compare(WritableComparable first, WritableComparable second) {
        TextIntWritable o1 = (TextIntWritable)first;
        TextIntWritable o2 = (TextIntWritable)second;
        if(! o1.getFirst().equals(o2.getFirst())) {
          return o1.getFirst().compareTo(o2.getFirst());
        }
        else {
          return o1.getSecond() - o2.getSecond();
        }
      }
  }
             
  // 自定义原生二次排序比较器，map输出的key中两个值都作为比较条件
  public static class TextIntRawComparator extends WritableComparator {
        private static final Text.Comparator TEXT_COMPARATOR = new Text.Comparator();
        private static final IntWritable.Comparator INTWRITABLE_COMPARATOR = new IntWritable.Comparator();
        public TextIntRawComparator() {
          super(TextIntWritable.class,true);
        }
           
        @Override
        public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
            try {
              int firstL1 = WritableUtils.decodeVIntSize(b1[s1]) + readVInt(b1, s1);
              int firstL2 = WritableUtils.decodeVIntSize(b2[s2]) + readVInt(b2, s2);
              int cmp = TEXT_COMPARATOR.compare(b1, s1, firstL1, b2, s2, firstL2);
              if( cmp != 0) {
                return cmp;
              }
              return INTWRITABLE_COMPARATOR.compare(b1, s1 + firstL1, l1 - firstL1, 
                  b2, s2 + firstL2, l2 - firstL2);
            } catch(IOException e) {
              throw new IllegalArgumentException(e);
            }
        } 
  }
             
  // 自定义反序列化后比较器，只比较map输出key的第一个值
  public static class TextComparator extends WritableComparator {
    public TextComparator() {
      super(TextIntWritable.class, true);
    }
    @SuppressWarnings("rawtypes")
      @Override
      public int compare(WritableComparable first, WritableComparable second) {
        TextIntWritable o1 = (TextIntWritable)first;
        TextIntWritable o2 = (TextIntWritable)second;
        return o1.getFirst().compareTo(o2.getFirst());
      }
  }
  // 自定义原生比较器，只比较map输出key的第一个值
  public static class TextRawComparator extends WritableComparator {
    private static final Text.Comparator TEXT_COMPARATOR = new Text.Comparator();
    public TextRawComparator() {
        super(TextIntWritable.class, true);
    }
           
    @Override
     public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
        try {
          int firstL1 = WritableUtils.decodeVIntSize(b1[s1]) + readVInt(b1, s1);
          int firstL2 = WritableUtils.decodeVIntSize(b2[s2]) + readVInt(b2, s2);
          int cmp = TEXT_COMPARATOR.compare(b1, s1, firstL1, b2, s2, firstL2);
          return cmp;
        } catch (IOException e) {
          throw new IllegalArgumentException(e);
        }
      }
  }
           
  // 自定义分区类， 以map输出数据key中的第一个值分区
  public static class ParationByText extends Partitioner<TextIntWritable, IntWritable> {
    @Override
      public int getPartition(TextIntWritable key, IntWritable val, int numParations) {
        int result = 0;
        switch(key.getFirst()) {
          case "Tom":
            result = 0;
            break;
          case "Lucy":
            result = 1;
            break;
          case "Green":
            result = 2;
            break;
          case "Lucius":
            result = 3;
            break;
          default:
            result = 4;
            break;
        }
        return result;
      }
           
  }
           
  @Override
    public int run(String[] args) throws Exception {
      Job job = Job.getInstance(getConf());
      job.setJarByClass(getClass());
      job.setJobName("SortByValue");
           
      job.setMapperClass(Map.class);
      job.setReducerClass(Reduce.class);
           
      job.setOutputKeyClass(Text.class);
      job.setOutputValueClass(Text.class);
           
      job.setMapOutputKeyClass(TextIntWritable.class);
      job.setMapOutputValueClass(IntWritable.class);
           
      job.setSortComparatorClass(TextIntRawComparator.class);
      //job.setSortComparatorClass(TextComparator.class);
      job.setGroupingComparatorClass(TextIntComparator.class);
      job.setPartitionerClass(ParationByText.class);
      job.setNumReduceTasks(5);
           
      job.setInputFormatClass(TextInputFormat.class);
      job.setOutputFormatClass(TextOutputFormat.class);
           
      FileInputFormat.addInputPath(job, new Path(args[0]));
      FileOutputFormat.setOutputPath(job, new Path(args[1]));
      return job.waitForCompletion(true) ? 0 : 1; 
    }
           
  public static void main(String[] args) throws Exception {
    ToolRunner.run(new SortByValue(), args);
  } 
}
```
