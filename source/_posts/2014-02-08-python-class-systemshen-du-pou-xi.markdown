---
layout: post
title: "Python Class System深度剖析"
date: 2014-02-08 01:02:10 +0800
comments: true
categories: [python]
---

## 1.序言

本文旨在说明：在Python里自定义class时，方法的第一个参数必须是该class的instance自身的引用(一般用self命名)。

在其他语言里，定义方法的时候，第一个参数不必是类实例的引用，一般约定俗成用this关键字来表示当前实例的引用，可是Python自成一派。由于网络上绝大部分文章都说成这是硬性规定，因此笔者觉得很有必要去研究一下Python里的class System是如何构筑起来的，并在此基础上说明self一词的作用。

## 2.面向对象编程

对象是数据和对数据的相关操作的封装。属于对象的数据与操作也可以称之为对象的属性（attributes）。对象具有层次构造，最下层的称之为instance，在其之上的称为class。class也具有层次构造，下层class会继承上层class的属性。有必要的时候可以再定义上层的属性。在Python里，一个class可以继承多个class（多重继承）。

## 3.Python里class的一般写法

首先，我们使用Python的class system来写一段程序。

IT公司“LiveGate”雇佣了大量的IT技术人员，并用Python写了一个管理技术人员信息的程序。理应用数据库来储存这些信息，但这里出于演示方便就不使用了。接着我们看一下表示IT技术人员的class的代码：

```python workers.py
#! /usr/bin/env python

"""
Workers in a IT company named LiveGate
"""

class Workers:
    """ This is a class of workers working in the company."""

    def __init__(self, name, position, email, age, salary):
        self.name = name
        self.position = position
        self.email = email
        self.age = age
        self.salary = salary


class ITWorkers(Workers):
    """ This is a class of IT engineers. """

    OS = 'WinNT'

    def __init__(self, language, *av):
        Workers.__init__(self, *av)
        self.language=language

    def work(self, n):
        """ IT engineers should work."""

        if self.position == 'web creator':
            w = 'makes web site'
        elif self.position == 'server administrator':
            w = 'checks the trafic'
        elif self.position == 'programmer':
            w = 'writes programs'

        print '%s %s for %d, hours using %s on %s' % (self.name, w, n, self.language, self.OS)

##------------------------------------------------------------------------------------------------
henley = ITWorkers('PHP', 'Henley', 'web creator', 'henley@livegate.com', 32, 700)
thomas = ITWorkers('Python', 'Thomas', 'server administrator', 'thomas@livegate.com', 37, 900)
gates  = ITWorkers('C', 'Gates', 'programmer', 'gates@livegate.com', 42, 1200)

henley.OS = 'Mac'
thomas.OS = 'Linux'

if __name__ == '__main__':

    henley.work(8)
    thomas.work(7)
    gates.work(10)
```

首先定义表示劳动者的class Workers（7--15行），接着定义它的子类ITWorkers（18--37行）。18行的ITWorkers(Workers)表示ITWorkers从Workers继承而来。ITWorkers从父类Workers继承其属性。Workers类的实例在初始化的时候，其实例变量：姓名，职业类别，e-mail地址，年龄，薪金将被存储起来。除此之外，ITWorkers会把使用语言（language）作为实例变量保存下来。出来传入language参数外，其他的参数由*av（元组）传递（当调用Workers.__init__时将其展开）。BTW，__init__是在实例构造完毕之后马上调用的专用方法(special method)（该专用方法是可选的，接近于其他OOP语言里的构造函数）。

接着，必须让IT技术人员按其薪金的多少来工作，因此定义方法work（25--33行）。work的第二个参数n表示工作时间。在这里，根据职业类型而分配其工作内容和工作时间，还有其使用的编程语言和操作系统类型。ITWorkers类里定义了类变量OS，其默认值为'WinNT'（19行）。也就是说，LiveGate公司里一般使用的操作系统是WindowNT。接着，我们定义3位IT技术人员，Henley, Thomas, Gates。Henley是Web开发者，作为一名艺术家，他使用Mac（44行）。Thomas是系统管理人员，工作上的关系，他使用Linux（44行）。编程人员Gates只要能用上编辑器（Editor）就可以了，对操作系统没什么特别要求，使用的是默认的'WinNT'。Henley, Thomas, Gates他们今天的工作时长为8, 7, 10个小时（43--45行）。

这里需要注意的是，为Henley和Thomas设定了不同的操作系统，则往他们的名字空间（namespace）里添加了OS这一项（entry）。由于Gates的名字空间里没有该条目，则往ITWorkers名字空间里搜寻。Henley和Thomas都能在自己的名字空间里找到OS，所以不用向上搜寻。同样的，因为work这一项不存在于每个IT技术人员的名字空间里，所以要往ITWorkers的名字空间里搜寻。

执行workers.py后，输出如下所示：

```bash
$ python workers.py
Henley makes web site for 8 hours, using PHP on Mac
Thomas checks the trafic for 7 hours, using Python on Linux
Gates writes programs for 10 hours, using C on WinNT
```

## 4.假如Python没有class system？

这里我们思考一下，假如Python没有class system，我们应该如何处理这种情况呢。当然，可以不使用OOP来写程序，但在这里，我们想创建属于自己的class system。

实际上，使用把函数当成数据一样来对待的编程语言（广义上指函数式语言）来创建OOP语言是非常简单的。可以使用hash表（Python里称字典）来表示各个对象的名字空间，对象的层次构造也可以根据hash表的层次结构来表示。由于Python也是把函数当成数据来对待，所以很容易实现OOP。

我们尝试用自己的class system来重新把workers.py写一遍。参考重新编写的代码，那您应该明白方法的第一个参数为什么是self了。

```python workers2.py
#! /usr/bin/env python

"""
This code demostrates how easy to imprement an object orientated system on a functional programming language.
It only requires a nested hash table.
"""


def Cls(cls=None, **key):
    """ making a new class"""
    key['class'] = cls
    return key

def new(cls, **key):
    """ making an instance """
    key['class'] = cls
    return key


def geta(obj, attr):
    """ getting the attribute of object """
    if attr in obj:
        return obj[attr]
    elif(obj['class']):
        return geta(obj['class'], attr)
    else:
        return None

def tell(obj, method, *av):
    """ tell object do something"""
    fun=geta(obj, method)
    if callable(fun):
        return fun(obj, *av)

if __name__=='__main__':

    def it_work(self, n):
        """This funciton demonstrates how IT engineers work.
           Notice that arguments of thie function is identical to the method 'work' in workers.py"""

        if geta(self, 'position') == 'web creator':
            w = 'makes web site'
        elif geta(self, 'position') == 'server administrator':
            w = 'checks the trafic'
        elif geta(self, 'position') == 'programmer':
            w = 'writes programs'

        print '%s %s for %d, hours using %s on %s' % (geta(self, 'name'), w, n, geta(self, 'language'), geta(self, 'OS'))

    workers = Cls() # dummy class
    it_workers = Cls(workers, OS='winNT', work=it_work) # class of IT workers

    henley = new(it_workers, language='PHP', name='henley',
                 position='web creator', email='henley@livegate.com', age=32, salary=700)
    thomas = new(it_workers, language='Python', name='Thomas',
                 position='server administrator', email='thomas@livegate.com', age=37, salary=900)
    gates  = new(it_workers, language='C', name='Gates',
                 position='programmer', email='gates@livegate.com', age=42, salary=1200)
    henley['OS'] = 'Mac'
    thomas['OS'] = 'Linux'

    tell(henley, 'work', 8)
    tell(thomas, 'work', 7)
    tell(gates, 'work', 10)
```

为了简化代码，workers2.py里并没有实现多重继承。

先看一下创建class的函数Cls和创建instance的函数new。实际上，两者是等同的，它们只是返回添加了表示父类'class'的hash表。

接下来看一下geta函数。这是一个搜索对象属性的函数。如果对象的hash表里没有目标属性，那么将递归地往父类的hash表里搜寻。这样一来，就能实现继承和重载（override）了。没有目标属性就向上一层搜寻，这样实现了属性的继承。假如下层的对象有定义该属性，则无视上层同名属性，这样实现了属性的再定义（override）。

函数tell告对象需要执行的方法。首先使用geta来搜索方法。然后如果找到的方法是函数（callable）的话，执行之，并返回结果。

这样便完成了定义class system的函数Cls, new, geta, tell。请注意它们都是简单定义的函数。

使用刚才创建好的class system，把workers.py重新写一次，如37行后面的代码所示。

先定义表示IT技术人员工作的函数it_work。请注意它的第一个参数是self。在函数it_work里使用geta来获取IT技术人员的属性。

接着，创建类it_workers时，把指向函数it_work的pointer赋值给其'work’属性。也就是说，hash表it_workers的'work'键(key)的值是指向it_work的pointer。只要能把函数当成数据来对待，就能够实现往hash表里填充函数。（译注：hash表里存储的是指向函数的引用）

跟workers.py一样，分别定义了3位IT技术人员。由于模拟专用发法__init__并不简单，所以这里在创建实例的时候，把IT技术人员的相关属性全都当成参数传递。接着使用函数tell使他们工作起来。输出的结果跟workers.py一样：

```bash
% python workers2.py
Henley makes web site for 8 hours, using PHP on Mac
Thomas checks the trafic for 7 hours, using Python on Linux
Gates writes programs for 10 hours, using C on winNT 
```

对比[workers.py]与[workers2.py]，可以看出他们相似的地方：

[workers.py]          [workers2.py]
obj.attribute     geta(obj, 'attribute')
obj.method(*av)   tell(obj, 'method', *av)
def work(self, n) def it_work(self, n)

这并不是偶然，Python里的class从原理上来说是这样实现的（请参考：Python reference manual 3.Data model）。实际上，Python已经为我们准备了跟函数geta一样功能的getattr函数。而且在特殊变量__dict__里定义了用于定义对象名字空间的hash表。我们可以试试在命令行里输入如下代码。粗体字是返回结果。

```python
>>> import sys
>>> from workers import *
>>> gates.__dict__
{'salary': 1200, 'name': 'Gates', 'language': 'C', 'age': 42, 'position': 'programmer',
'email': 'gates@livegate.com'}
>>> henley.__dict__
{'salary': 700, 'name': 'Henley', 'language': 'PHP', 'age': 32, 'position': 'web  creator',
'OS': 'Mac', 'email': 'henley@livegate.com'}
>>> ITWorkers.__dict__
{'__module__': 'workers', 'work': <function work at 0x00A34630>, 'OS': 'WinNT',
'__doc__': ' This is a class of IT engineers. ', '__init__': <function __init__
at 0x00A345F0>}
>>> ITWorkers.work(gates, 10)
Gates writes programs for 10, hours using C on WinNT
>>>  gates.__class__.work(gates, 10)
Gates writes programs for 10, hours using C on WinNT
>>> getattr(henley, 'OS')
'Mac'
>>> getattr(henley, 'work')
<bound method ITWorkers.work of <workers.ITWorkers instance at 0x00A33760>>
>>> getattr(henley, 'work')(8)
henley makes web site for 8 hours, using PHP on Mac
```

导入sys和workers模块，然后试试敲进上面的8行命令。Gates的名字空间里（[1]）有各类项目(entry)，但是没有'OS'这一项。Henley的名字空间里（[2]）有定义'OS'。ITWorkers的名字空间里（[3]）除了预置的__module__, __doc__，还有我们定义的'OS'，work，__init__ 。特别的，方法（<function work at 0x00A34630>）作为函数被保存在内存里。正如前面提及的一样，Henley使用自身定义的'OS'，而Gates则向上搜寻，使用类ITWorkers里的'OS'（'OS'不存在Gates这个名字空间里）。

因为在类ITWorkers里定义了函数work，我们可以试着像[4]那样直接调用它。其输出跟调用gates.work(10)一样。由于每个实例有一个内置属性，__class__，它指向该实例所属的类，所以我们可以像[5]那样调用方法且得到相同的结果。

最后我们试试getattr函数。像[6]那样，getattr(henley, 'OS')得到的结果跟henley.OS一样。我们把它应用到方法上看看（[7]）。返回如下结果：

```xml
<bound method ITWorkers.work of <workers.ITWorkers instance at 0x00A33760>>
```

`<workers.ITWorkers instance at 0x00A33760>`这是Henley在内存中的地址。这个函数不用'function'而用'bound method'表示。其实'bound method'可以像[8]那样调用。这就说明了为什么从外部调用类方法的时候，第一个参数不必是实例自身的引用。不过，真正的理由应该是那样的做法不够酷:p。'bound method'可以看做是[workers2.py]里tell函数的语法糖（構文糖衣）。

从上面可以看出，在函数式语言里引入class system时，作为方法而定义的函数，很自然地，需要指向实例的参数。通过def关键字，在类里面定义过程与定义普通的函数一样，只是其作用域被限定在class里面。所以定义方法的第一个参数必须是self，否则不能引用实例里的变量。


## 5.结语

Python基本上是函数式语言（广义的），面向对象是其使用hash表后的附属物而已。这一点与原本作为面向对象编程语言而设计的C++, Java, Ruby等相异。

Python把过程的定义合并成函数的定义（没有将函数的定义与方法的定义区分开来），但在定义方法时，第一个参数必须是实例的引用。这是约定俗成的。

函数式语言要比面向对象语言更加抽象。Python深受函数式语言Haskell的影响。实际上，像[workers2.py]所示的那样，函数式语言可以简单地实现面向对象编程。

------------------------------------------------------------------------------

参考:

[Python Class System](http://www.cnblogs.com/chenzehe/archive/2010/08/18/1802701.html)

