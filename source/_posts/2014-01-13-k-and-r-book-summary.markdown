---
layout: post
title: "K&amp;R Book Summary"
date: 2014-01-13 10:21:11 +0800
comments: true
categories: [coding]
keywords: [clanguage]
---
## chapter 1 (导言）

* data types
 int 2 bytes

 float 4 bytes

 double 8 bytes

 char 1 byte
 
 8 bit,signed -128-127

 8 bit unsigned 0-255

 16 bit signed -32768-32767

 16bit unsigned 0-65536

 32bit signed -2147483648-2147483647

 32bit unsigned 0-4294967295
 
* “!=" 优先级比"="高

* printf 

 %6d 按照十进制打印，至少6个字符宽

 %6f 按照浮点数打印，至少6个字符宽

 %.2f 按照浮点数打印，小数点后有2位

 %6.2f 按照浮点数打印， 至少6个字符宽， 小数点后有两个小数位

 %.0f 强制不打印小数点和小数部分

 double, float都使用%f说明
 
* 单引号中的字符表示一个[整形值],称为字符常量(A -> ASCII字符集中值为65， A的意义更清楚, '\n' -> '10')

* 外部变量不论实在多个源文件中还是在本源文件中，只要出现在使用位置之后，都需要加extern

## chapter2 (类型，运算符，表达式)

* 变量名

 内部变量名：至少前31个字符有效

 外部变量名和函数名：ANCI遍准只保证前6个字符唯一

* 基本数据类型

 **字符型**  `char` 1 byte

 **整型** `int`  所用机器中整数的最自然长度

 **浮点数**  `float` 单精度； `double` 双精度

**长度限定词**`short int`, `long int`

**类型限定词**`singed char`, `unsigned char`, `signed int`, `unsigned long`, etc

* 惯例

`short` 16位

`long`  32位

`int`长度不定，但是遵循以下限制: `short 至少16位, long 至少32位，short <= int <= long`

限定属性在标准头文件<limits.h>和<float.h>中找到

* 常量

举例：

`int: 1234`, `long: 12345L 或 123456l`

`unsigned long: 123456ul 或者 123456UL`

float,double类型常包含小数点或者一个指数或者二者都有,没有后缀的为double型， 有后缀的为float型: 

`double: 123.4, 1e-2, 1.23e-3`, `float: 123.4f, 1e-2f, 1.23e-3`

`long double: 123.4l`, `long double: 123.4L`

**转义字符序列** 表示为**字符和字符串常量**。

`\xooo`： ooo表示1-3个八进制数字

`\xhh`： hh表示一个或多个十六进制数字

**常量表达式**  只包含常量的表达式，在编译时求值，*可以出现在任何普通常量出现的位置

```c
  #define MAXLINE 1000
  char line[MAXLINE]
```
**字符串常量** 也叫字符串字面量 

```c
  "hello，”“ruby"
```
字符串常量的连接如上.

**枚举常量**
枚举是一个常量整数型的列表，如：

```c
  enum boolean {NO, YES};
```

在没有显示说明的情况下，enum类型中第一个枚举名的值位0，第二个为1,以此类推. 如果只指定了其中一部分值，未指定值的枚举名的值将
依着最后一个制定值向后递增.

不同枚举的变量名必须互不相同， 同一枚举中不同的名字可以有相同的值.

