---
layout: post
title: "python 方法类模块和包总结"
date: 2014-02-05 01:47:03 +0800
comments: true
categories: [python]

---
## Python 类和模块的关系

类总是位于模块中，类是模块对象的属性。

类和模块都是命名空间，但类对应于语句而不是整个文件. 类支持多个实例、继承以及运算符重载这些OOP概念。

总之，模块就像是一个单一的实例类，没有继承，而且模块对应于整个文件的代码。

## 一. 函数

要点： python 函数参数的传递(参数带星号的说明)

**1.  F(arg1,arg2,...)**

最常见形式，参数以逗号分割。函数调用时传入参数个数和函数定义的参数个数必须保持一致！

```python
>>> def f(x,y):
...     print x,y
... 
>>> f(1,2)
1 2
>>> f(1) # 定义参数两个，传入参数一个，报错!
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  TypeError: f() takes exactly 2 arguments (1 given)
```
**2.  F(arg1,arg2=value2,...)**

提供了部分默认值的函数，调用时传入的实参 个数不必和函数定义时的个数一致

注，默认参数必须放在最后，否则会报错！

```python
>>> def f(x,y):
...     print x,y
... 
>>> f(1,2)
1 2
  >>> def f(x,y=3,z=8): # 定义3个参数，后两个提供默认值
  ...     print x,y,z
  ... 
  >>> f(1) # 传入1个 
  1 3 8
  >>> f(1,4) # 传入2个，第二个覆盖默认值
  1 4 8

  >>> def f(x,y=3,z): # 定义3个参数，中间为默认值，最后一个为no-default参数
  ...     print x,y,z # 报错！ 提供默认值参数后边不能跟无默认值参数！
  ... 
    File "<stdin>", line 1
    SyntaxError: non-default argument follows default argument
```

**3.  F(*arg1)**

传入参数个数不定，0个或者多个，传入的参数在函数内部存放在以**函数形参**命名的**tuple**中

```python
>>> def f(*x):
...     if len(x) == 0:
...             print 'None';
...     else:
...             print x
... 
>>> f()
None
>>> f(1,2,3)
(1, 2, 3)
>>> f('a','b','c','d',1,2,3)
('a', 'b', 'c', 'd', 1, 2, 3)
```

**4.  F(**arg1)**

传入参数个数不定，0-n个，传入的参数放在**字典**(dictionary)中, 形参位dict的key, 传入的参数值为对应的value.

传入的参数不一定按照形参定义的次序，但是必须以**键值对key=val**的形式传参

```python
>>> def f(**x):
...     if len(x) == 0:
...             print 'None'
...     else:
...             print x
... 
>>> f(x = 1, y = 8, z = 3)
{'y': 8, 'x': 1, 'z': 3}
```

**注意** 此种方式定义的函数，**形式参数的名**也是不定的，根据传入的键值对来确定。

**5.  综合以上四种**

```python
>>> def f(x, y = 2, *a, **b):
...     print 'x=', x, 'y=', y, 'a:', a, 'b:', b
... 
>>> f(1,3, 8,88,888)
x= 1 y= 3 a: (8, 88, 888) b: {}
>>> f(1,3, 8,88,888, b1=3,b2=9)
x= 1 y= 3 a: (8, 88, 888) b: {'b1': 3, 'b2': 9}
```

## 二.  模块

模块在python中可以理解为一个文件，定义一些function, 变量，在其它需要这些功能的文件中导入这个模块，就可重用function和这些变量。

常用的调用方法：`modulename.funname`, `modulename.varname`

```python module1.py
def say(word):
    print word
```

```python caller.py
import module1

print __name__, ' in caller.py'
print module1.__name__, ' in caller.py'
module1.say('hello')
```

运行结果:

```bash
% python caller.py 
python caller.py 
__main__  in caller.py
module1  in caller.py
hello
```

### 2.1  模块的属性**__name__**

每个模块都有自己的`__name__`, __name__作为模块的内置属性, 它的值由python解释器设定，

模块调用方式有**直接执行**和**import 载入调用**， **直接执行**时，`__name__ == __main__`,
否则，`__name__ == “module name"`

这个属性非常有用，常可用来进行模块内置测试使用, 可以指定模块内部某些语句只有**该模块作为主程序调用**时才执行。

举例：

#### 2.1.1  直接运行module

```python  module_test.py
#/usr/bin/env python

class A:
  count = 0 
  def __init__(self, name):
    self.name = name
    if(__name__=="__main__"):
      print "the module is execting by itself!"
    else:
      print "the module is being exected by another program!"

  def GetInfo(self):
    print self.name

a = A("test")
a.GetInfo()
```
运行结果：

```bash 
% python module_test.py
the module is execting by itself!
test
```

#### 2.1.2  间接调用module

```python module_user.py
import module_test

print '------------'
print 'below we can use the imported module'
a = module_test.A("user")
a.GetInfo()
```

运行结果：

```bash
% python module_user.py
the module is being exected by another program!
test
------------
below we can use the imported module
the module is being exected by another program!
user
```

#### 2.1.3  导入模块中的部分类

举例：

```python
#!/usr/bin/env python
from module_test import A

.
.
.
```

###  2.2  模块搜索路径

python解释器搜索路径：

**当前目录** -> **环境变量PYTHONPATH** -> **安装时的预设目录**

####  动态修改搜索路径**

主要使用list的append()或insert()增加新的目录:


```python  add_search_path.py
#!/usr/bin/env python
import sys
import os

print sys.path
print '---------------------'
print 'add a search path as following'
workpath = os.path.dirname(os.path.abspath(sys.argv[0]))
sys.path.insert(0, os.path.join(workpath, 'modules'))
print sys.path
```
运行结果：

```bash
 python add_search_path.py 
['/home/luciuz/Code/python', '/usr/lib/python26.zip', '/usr/lib/python2.6', '/usr/lib/python2.6/plat-linux2', '/usr/lib/python2.6/lib-tk', '/usr/lib/python2.6/lib-old', '/usr/lib/python2.6/lib-dynload', '/usr/lib/python2.6/site-packages', '/usr/lib/python2.6/site-packages/PIL', '/usr/lib/python2.6/site-packages/gst-0.10', '/usr/lib/python2.6/site-packages/gtk-2.0', '/usr/lib/python2.6/site-packages/setuptools-0.6c11-py2.6.egg-info', '/usr/lib/python2.6/site-packages/webkit-1.0']
---------------------
add a search directory as following
['**/home/luciuz/Code/python/modules**', '/home/luciuz/Code/python', '/usr/lib/python26.zip', '/usr/lib/python2.6', '/usr/lib/python2.6/plat-linux2', '/usr/lib/python2.6/lib-tk', '/usr/lib/python2.6/lib-old', '/usr/lib/python2.6/lib-dynload', '/usr/lib/python2.6/site-packages', '/usr/lib/python2.6/site-packages/PIL', '/usr/lib/python2.6/site-packages/gst-0.10', '/usr/lib/python2.6/site-packages/gtk-2.0', '/usr/lib/python2.6/site-packages/setuptools-0.6c11-py2.6.egg-info', '/usr/lib/python2.6/site-packages/webkit-1.0']
```

### 2.3 modules放在目录下

说明：

modules所在的目录在python里叫`package`, 下面是一个名为 `IsDir`的package(实际上就是一个目录), package下面有4个`modules`(A, B, C, D)和一个`__init__.py`文件，目录结构如下：

```bash
IsDir/
A.py  B.py  C.py  D.py __init__.py
```
大体来讲，有两种方法可以调用某目录下（包括递归目录）的modules.

#### 2.3.1  __init__.py为空时

1.1 以下为调用moduleA的代码：

```python
#!/usr/bin/env python
from IsDir import A
A.say()
```

输出：

`This is module A!`

1.2 如果想调用moduleA,B,C,D呢？

方法1.

```python
#!/usr/bin/env python
from IsDir import A
from IsDir import B
from IsDir import C
from IsDir import D
A.say()
B.say()
C.say()
D.say()
```

方法2.

```python
#!/usr/bin/env python
import IsDir.A
import IsDir.B
import IsDir.C
import IsDir.D
from IsDir import *
A.say()
B.say()
C.say()
D.say()
```

错误示例1：

```python
#!/usr/bin/env python
import IsDir.A
A.say()
```

错误示例2：

```python
#!/usr/bin/env python
from IsDir import *
A.say()
```

错误的原因：

`IsDir/`目录下`__init__.py`为空时，直接`import IsDir.A`或者`from IsDir import *`是无效的.

从官方文档里可以看到，`__init__.py`里没有`__all__ = [module1,module2,...]`时，

`from IsDir import *`只能保证`IsDir`被`imported`,所以此时`IsDir`里的modules是无法被imported的,
此时只有如我上面所写的代码所示才能正确执行，否则是错误的。官方解释为：`import IsDir.A`并无任何意义，只有接着执行`from IsDir import *`后，`import IsDir.A`语句里的`module A`才会被定义，所以完整的调用因改为: 1.  `import IsDir.A`   2. `from IsDir import *`。

### 2.3.2   __init__.py用all=[...]指定该package下可以被imported进去的module

`__init__.py`里写入如下内容：

```sh
% cat IsDir/__init__.py
__all__ = ["A","B"]
```

然后使用之：

```python
#!/usr/bin/env python
from IsDir import *
A.say()
B.say()
```

结果：

```sh
% python test.py 
This is module A!
This is module B!
```

错误实例：

```python
#!/usr/bin/env python
from IsDir import *
C.say()
```

以上示例之所以错误，是因为`C`并没有在`__all__ = ["A","B"]`里制定，由此可见，package `IsDir`下面的`__init__.py`里，`__all__=[...]`具有隔离modules的作用。


补充:

module A, B, C,D里我分别只定义了一个method, 例如，以下为`module A`的code:

```sh
% cat IsDir/A.py
def say():
  print "This is module A!"
```

## 三.  类

**构造函数** `__init__() 方法`

**折构函数** `__del__() 方法`


### 3.1 构造函数__init__()解析

构造函数的定义形式如下：

`__init__(self, argv1, argv2)`



举例：

```python test_init.py
#!/usr/bin/env python

class A:
  def __init__(what1, var1, var2):
    print what1, var1, var2

  def getValue(what2, val3):
    print what2, val3

a = A('foo', 'bar')
a.getValue('qux')
```

运行结果：

```bash init_test.py
% python init_test.py
<__main__.A instance at 0xb76f4bcc> foo bar
<__main__.A instance at 0xb76f4bcc> qux
```

**构造函数**实际上并不是构造器，只是_init__()函数在类A被调用是隐式调用。

传给类A的参数和传给__init()__的完全一致。

### 3.2**类的变量**与**对象的变量**

```python
class A:
  numb = 0 # 类的数据
  def setData(self, digit):
    self.numb = digit # 每个实例的数据
    self.count = digit + 1 # 每个实例的数据

```

### 3.3 理解类的概念,类的数据与实例的数据:

python里，类是产生实例的模板。 对类的调用可以产生实例对象。

新产生的实例只是一个名字空间，仅包含从类中继承而来的属性。

类的方法中对self的赋值改变实例的属性

### 3.3 实例的创建过程分析(例子见3.2)

```python
a = A()
a.setData(1)
b = A()
b.setData(3)
```

以上两句将产生两个实例，各自有不同的命名空间。 此时有三个对象——两个实例对象和一个类对象本身。
此时会产生三个连接起来的命名空间。

实例一开始是空的，仅有指向产生它的类本身的链接。

当实例a或者b调用方法setData()时，因实例本身并没有此方法，因此会向上查找，最终会调用类的方法setData()，
实例对象会通过类的方法里的第一个参数self传入，因此通过self.numb的赋值实际上是对实例本身的赋值

**覆盖**： 当类的变量名和类的方法中的变量名相同时，通过实例调用的是方法体内部的变量名

**通过实例修改类变量** 通过实例修改类变量foo，该实例对象将得到类变量的拷贝，不再与类变量共享同名变量foo

```python
#!/usr/bin/env python

class A:
  numb = 0 # 类的数据
  foo = 111
  def __init__(self,digit ):
    self.numb = digit #通过self对实例的数据赋值
    self.count = digit + 1

  def add(self, d):
    self.numb += d

a = A(1) # 调用类，产生新的实例，有自己的命名空间
print '-------------------------'
print "a=A(1), a.numb:",a.numb # 隐式调用__init__()
print "a.count:",a.count
a.add(2) # 调用方法add(),实例中没有，向上查找，在类里调用，实例对象会传给self
print "a.add(2), a.numb:",a.numb
print "通过实例修改前, 实例a与类共享变量foo，a.foo: ", a.foo # 通过实例对象来调用类变量
a.foo += 100 # 通过实例对象来修改类变量，将得到该类变量的单独拷贝，此后该实例将不再与类共享同名的变量
print "通过实例修改类变量后，实例与类不再共享此同名变量，a.foo: ", a.foo # 通过实例对象来调用类变量
print '-------------------------'

b = A(100)
print "b=A(100), b.numb:",b.numb # a,b实例命名空间相互独立，变量同名，保存的数据不同
print "b.count:",b.count
print "实例对象b中未修改类变量foo,故该实例b和类A共享类变量foo, b.foo:", b.foo
print '-------------------------'

print "A.numb:", A.numb
A.numb += 9999 # 对类的数据赋值，而不是对实例的对象赋值
print "A.numb+=9999, A.numb", A.numb
print '-------------------------'

c = A(200)                        # 类的数据和实例的数据相互独立，同名也不会冲突
print "c=A(200), c.numb:", c.numb # 因此，虽然执行了A.numb+=9999但是不会影响实例的数据 
print "c.count:",c.count         

print "A.numb:", A.numb

```

**运行结果**：

```bash
$ python test.py 
-------------------------
a=A(1), a.numb: 1
a.count: 2
a.add(2), a.numb: 3
通过实例修改前, 实例a与类共享变量foo，a.foo:  111
通过实例修改类变量后，实例与类不再共享此同名变量，a.foo:  211
-------------------------
b=A(100), b.numb: 100
b.count: 101
实例对象b中未修改类变量foo,故该实例b和类A共享类变量foo, b.foo: 111
-------------------------
A.numb: 0
A.numb+=9999, A.numb 9999
-------------------------
c=A(200), c.numb: 200
c.count: 201
A.numb: 9999
```

### 3.4 类的调用与实例调用之间的关系

Python自动把对实例的调用转换为对类的方法的调用:

`instance.method(args...) <=> class.method(instance, args...)`

举例：

(主要区别在于通过具体实例调用类的方法时，实例对象会自动被传给方法的第一个参数)

```python
>>> class A:
...     def display(self,value):
...             print value
... 
>>> a = A()
>>> a.display("hello,python!") # 通过实例调用类的方法
hello,python!
>>> A.display(a,"hello,python!") # 通过类显式调用类的方法,二者等价
hello,python!
```


## 四.  包

模块过多，将功能相近的模块放在同意目录下，此文件夹便称为包。

**注意**  文件夹当作包使用时，该文件夹需要包含__init__.py的文件， **目的**是为避免将文件夹名当作普通字符串。

__init__.py文件可以为空，一般用来进行包的初始化工作，或者设置__all__值。

__all__值主要用在`from package-name import *`,用来全部导出定义过的模块。


