---
layout: post
title: "xargs的用法总结"
date: 2014-02-01 16:46:38 +0800
comments: true
categories: [linux]
---

xargs的作用等同于大多数Unix shell中的反引号，但更加灵活易用，并可以正确处理输入中有空格等特殊字符的情况。

例如：

```bash
find . -name "*.foo" | xargs grep bar
```
等同于：

```bash
grep bar `find . -name "*.foo"`
```
用法举例：

批量查找并删除文件

```bash
find /path -type f -print0 | xargs -0 rm
```

xargs将find产生的长串文件列表拆散成多个子串，然后多每个子串调用rm。

其中，-print0表示每个输出以null分割，-print 表示用换行符输出，-0表示輸入以null分隔。

这样要比如下使用find命令效率高的多。

*旧版*：

```bash
find /path -type f -exec rm '{}' \;
```

*新版*：

```bash
find /path -type f -exec rm '{}' +
```

批量查找并用vi打开：

```bash
find . -name "*.foo" -print0 | xargs -0 -t -r vi
```

批量查找并修改文件名：

```bash
find . -name "*.foo" -print0 | xargs -0 -i mv {} /tmp/trash
```

使用-i参数将{}中内容替换为列表中的内容。
