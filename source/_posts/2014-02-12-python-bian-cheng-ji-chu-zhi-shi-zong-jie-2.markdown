---
layout: post
title: "python 编程基础知识总结(二)"
date: 2014-02-12 17:52:01 +0800
comments: true
categories: [python]
---

sequence相关的函数, 文件操作总结, 以及built-in functions, built-in types官方文档

#  目录

1. trip lstrip rstrip使用方法  

2. 序列相关的built-in函数

3. 文件基本操作续

#  正文

## 一.  trip lstrip rstrip使用方法

```python
>> str = '#this is a demo#'
>>> print str.strip('#') # strip()
this is a demo
>>> str = 'abcd#this is a demo#abc'
>>> print str.strip('abc') # 去首尾['a','b','c']数组
d#this is a demo#
>>> print str.lstrip('#') # lstrip()
this is a demo#
>>> print str.rstrip('#') # rstrip()
this is a demo
```

## 二.  序列相关的built-in函数

基本函数有: sorted(), reversed(), enumerate(), zip()

其中sorted()和zip()返回一个序列(列表)对象，reversed()、enumerate()返回一个迭代器


### 2.1 sorted()与list.sort() 

内建函数(built-in function) sorted(iterable[, cmp[, key[, reverse]]])

等价于：可变序列(Mutable Sequence 类型) list.sort([cmp[, key[, reverse]]])

sort方法接受三个参数，都可以省略，默认是升序排序。

```python
# list.sort) demo
>>> l = ['f', 'a', 8, 1, 'e']
>>> l.sort()
>>> print l
[1, 8, 'a', 'e', 'f'] # 默认升序
>>>
>>> l.sort(reverse=True)
>>> print l
['f', 'e', 'a', 8, 1]
>>>
>>> l.sort(cmp=lambda a,b : cmp(a,b)) 
>>> print l
[1, 8, 'a', 'e', 'f']
>>> l.sort(cmp=lambda a,b : cmp(b,a)) # cmp()输入的第一个数小于第二个数则返回-1
>>> print l
['f', 'e', 'a', 8, 1]
>>> d = [9,2,1,4,3]
>>> d.sort(key=lambda a:-a)
>>> print d
[9, 4, 3, 2, 1]
# 注意sort()中key的妙用
>>> str = ['abcde', 'ab', 'acde', 's', 'aceg']
>>> str.sort(key=len)
>>> print str
['s', 'ab', 'acde', 'aceg', 'abcde'] # 按字符串长度排序

# sorted()demo
>>> d = [9,2,1,4,3]
>>> print sorted(d,reverse=False)
[1, 2, 3, 4, 9]
>>> print sorted(d,reverse=True)
[9, 4, 3, 2, 1]
>>> print sorted(d, cmp=lambda a,b: cmp(a,b))
[1, 2, 3, 4, 9]
```

### 2.2  reversed()与list.reverse()

reversed()直接对传入的list修改

list.reverse()返回list

```python
# reversed()demo

>>> list = [1, 2, 3, 4, 9]
>>> reversed(list)
<listreverseiterator object at 0xb76bbd2c>
>>> iter = reversed(list) # 返回迭代器
>>> for i in iter:
...     print i,
... 
9 4 3 2 1

>>> for i in reversed(list): # 推荐使用法
...     print i,
... 
9 4 3 2 1

# list.reverse()demo
>>> list = [1, 2, 3, 4, 9]
>>> list.reverse()
>>> print list
[9, 4, 3, 2, 1]
```

### 2.3 枚举函数enumerate()

enumerate(sequence, start=0)

A new built-in function, enumerate(), will make certain loops a bit clearer. enumerate(thing), where thing is either an iterator or a sequence, returns a iterator that will return (0, thing[0]), (1, thing[1]), (2, thing[2]), and so forth.

A common idiom to change every element of a list looks like this:

```python
>>> seasons = ['Spring', 'Summer', 'Fall', 'Winter']
>>> for i in range(len(seasons)):
...     print '%d:' %i, seasons[i],
... 
0: Spring 1: Summer 2: Fall 3: Winter
```
This can be rewritten using enumerate() as:

```python
# demo1 default
>>> seasons = ['Spring', 'Summer', 'Fall', 'Winter']
>>> e = enumerate(seasons)
>>> for index,season in e:
...     print '%s:%s '%(index,season),
... 
0:Spring  1:Summer  2:Fall  3:Winter 
```

# demo2 start index is not zero

```python
>>> seasons = ['Spring', 'Summer', 'Fall', 'Winter']
>>> e = enumerate(seasons,start = 2)
>>> for index,season in e:
...     print '%s:%s '%(index,season),
... 
2:Spring  3:Summer  4:Fall  5:Winter 
>>> 
```

### 2.4  zip()

l1: (x1,x2,x3) l2:(y1,y2,y3,y4) 

zip()功能：

zip（l1,l2)-> [(x1,y1),(x2,y2),(x3,y3)]

传入参数的长度不等，则返回list的长度和参数中长度最短的对象相同。

简单来说，即将若干可迭代对象的元素一一对应，压缩为一个个元组

```python
# demo1 压缩
>>> z1 = ['a','b','c']
>>> z2 = [1,2,3]
>>> result = zip(z1,z2)
>>> result
[('a', 1), ('b', 2), ('c', 3)]

# demo2 解压
>>> zip(*result)
[('a', 'b', 'c'), (1, 2, 3)]
```

zip()的应用举例：

#### 2.4.1 二维矩阵变换（矩阵的行列互换)

```python

# 假设有二维矩阵a
>>> a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]] 

# 方法一 通过[列表推导]得到最终行列互换后的结果
>>> print [ [row[col] for row in a] for col in range(len(a[0]))] 
[[1, 4, 7], [2, 5, 8], [3, 6, 9]]
```

# 方法二 利用zip函数

```python
>>> a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
>>> zip(*a)
[(1, 4, 7), (2, 5, 8), (3, 6, 9)]
>>> map(list,zip(*a))
[[1, 4, 7], [2, 5, 8], [3, 6, 9]]
```
这种方法速度更快但也更难以理解，将list看成tuple解压，恰好得到我们“行列互换”的效果，再通过对每个元素应用list()函数，将tuple转换为list


**以指定概率获取元素**

```python
>>> import random
>>> def random_pick(seq,probabilities):
  x = random.uniform(0, 1)
  cumulative_probability = 0.0
  for item, item_probability in zip(seq, probabilities):
    cumulative_probability += item_probability
    if x < cumulative_probability: break
  return item

>>> for i in range(15):
  random_pick("abc",[0.1,0.3,0.6])
  
'c'
'b'
'c'
'c'
'a'
'b'
'c'
'c'
'c'
'a'
'b'
'b'
'c'
'a'
'c'
```

这个函数有个限制，指定概率的列表必须和元素一一对应，而且和为1，否则这个函数可能不能像预想的那样工作。
稍微解释下，先利用random.uniform()函数生成一个0-1之间的随机数并复制给x，利用zip()函数将元素和他对应的概率打包成tuple，然后将每个元素的概率进行叠加，直到和大于x终止循环
这样，”a”被选中的概率就是x取值位于0-0.1的概率，同理”b”为0.1-0.4，”c”为0.4-1.0，假设x是在0-1之间平均取值的，显然我们的目的已经达到

**转置后再还原**

```python
>>> x=[1,2,3]
>>> y=['a','b','c']
>>> zip(x,y)
[(1, 'a'), (2, 'b'), (3, 'c')]
>>> map(list,zip(*zip(x,y)))
[[1, 2, 3], ['a', 'b', 'c']]
```
以上这个纯属无聊瞎写，旨在加深理解


#### 2.4.2 代码引导

demo1:

```python
>>> name = ('jack','beginman','sony','pcky')
>>> age = ('1987', '1988', '1998', '1983')
>>> zip(age,name)
[('1987', 'jack'), ('1988', 'beginman'), ('1998', 'sony'), ('1983', 'pcky')]
>>> for a,n in zip(age,name):
...     print 'age: %s' %a, 'name: %s' %n
... 
age: 1987 name: jack
age: 1988 name: beginman
age: 1998 name: sony
age: 1983 name: pcky
```
对比传统从字典里提取：

```python
>>> str = {'jack':1987, 'beginman':1988, 'sony':1998, 'pcky':1983}
>>> print str
{'sony': 1998, 'pcky': 1983, 'jack': 1987, 'beginman': 1988}
>>> print str.keys()
['sony', 'pcky', 'jack', 'beginman']
>>> for key in str.keys():
...     print key, str[key],
... 
sony 1998 pcky 1983 jack 1987 beginman 1988
```

## 三. 对文件的基本操作

说明： 和perl一样，python内置函数打开磁盘上的文件并返回文件句柄来执行后续操作 

open()接受三个参数(open接收三个参数，文件名、模式、缓冲区参数)，后两个可选，默认打开模式为'r'只读

### 3.1 文件管理中的tell、seek、truncate  

1) seek(offset,where):  where=0从起始位置移动，1从当前位置移动，2从结束位置移动。当有换行时，会被换行截断。seek（）无返回值，故值为None。

2) tell():  文件的当前位置,即tell是获得文件指针位置，受seek、readline、read、readlines影响，不受truncate影响

3) truncate(n):  从文件的首行首字符开始截断，截断文件为n个字符；无n表示从当前位置起截断；截断之后n后面的所有字符被删除。其中win下的换行代表2个字符大小。

4) readline(n):读入若干行，n表示读入的最长字节数。其中读取的开始位置为tell()+1。当n为空时，默认只读当前行的内容

5) readlines读入所有行内容

6) read() 若无参数，则读入所有行内容, 有参数则读取指定字节的数据

demo:

```python
>>> f = open("demo1.txt",'w+') # 打开写权限
>>> print f.tell() # tell()返回的是文件指针所指的当前位置
0
>>> f.write("abc\ndef") # 写入3个字符
>>> print f.tell() # 此时文件指针指向距开始处7个单位处
7
>>> f.seek(1,0) # 文件指针移动，0表示从文件开始出移动，1表示移动1个字符
>>> print f.tell()
1
>>> print f.readline() # 读取当前行, 从指针所指的位置开始读取（1个单位处）
'bc\n'
>>> print f.tell() # 文件指针指向距开始处3个单位
4
>>> f.readline() # 读取下一行
'defhi\n'

# read()和readlines()的区别
>>> f.seek(0,0)
>>> f.read() 
'abc\ndefhi\n'
>>> f.seek(0,0)
>>> f.readlines()
['abc\n', 'defhi\n']
```
# truncate的用法

```python
>>> f.write("\ndef\nghi\n")
>>> f.seek(0,0)
>>> f.read() # 原始文件有3行
'abc\ndef\nghi\n'
>>> f.seek(0,0) # 文件指针移到文件气势位置
>>> f.truncate(5) # 保留起始位置开始前5个字符，后面的所有全部删除
>>> f.seek(0,0)
>>> f.read() # 默认读取全文
'abc\nd'
>>> f.seek(0,0)
>>> f.read(3) # 指定读取3个字符
'abc'
```

### 3.2 序列化数对象保存到文件

```python
>>> f = open('demo2.txt', 'w')
>>> f.readlines()
[]
>>> >>> list = [('a','foo'), [1,'nani'],'bar', 88]
>>> import pickle
>>> pickle.dump(list,f)
>>> f.close()
>>> f_obj = open('demo1.txt','r')
>>> print pickle.load(f_obj)
[('a', 'foo'), [1, 'nani'], 'bar', 88]
```

## 四. Built-in Types

None, False; and, or, not; >=, !=, is not;

int, float, long, complex; x | y, x >> n;

int.bit_length(), float.as_integer_ratio(), float.hex()

container.__iter__(), terator.__iter__(), iterator.next()

str, unicode, list, tuple, bytearray, buffer, xrange

set, frozenset;  dict; file.open(), file.flush(), file.read()

memoryview, bytearray; contextmanager.__enter__(), contextmanager.__exit__(exc_type, exc_val, exc_tb)

modules, functions, Methods, Code Objects, Type Objects, Null Objects, The NotImplemented Object, The Ellipsis Object,Internal Objects, Special Attributes

参考:

5. Built-in Types: http://docs.python.org/2/library/stdtypes.html#typesseq-mutable

## 五. Built-in Functions

abs()  divmod()  input()   open()  staticmethod()
all()   enumerate()   int()   ord()   str()
any()   eval()  isinstance()  pow()   sum()
basestring()  execfile()  issubclass()  print()   super()
bin()   file()  iter()  property()  tuple()
bool()  filter()  len()   range()   type()
bytearray()   float()   list()  raw_input()   unichr()
callable()  format()  locals()  reduce()  unicode()
chr()   frozenset()   long()  reload()  vars()
classmethod()   getattr()   map()   repr()  xrange()
cmp()   globals()   max()   reversed()  zip()
compile()   hasattr()   memoryview()  round()   __import__()
complex()   hash()  min()   set()   apply()
delattr()   help()  next()  setattr()   buffer()
dict()  hex()   object()  slice()   coerce()
dir()   id()  oct()   sorted()  intern()

参考:

2. Built-in Functions: http://docs.python.org/2/library/functions.html#list
