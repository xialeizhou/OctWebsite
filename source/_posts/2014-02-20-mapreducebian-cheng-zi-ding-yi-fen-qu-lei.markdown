---
layout: post
title: "mapreduce编程自定义分区类"
date: 2014-02-20 02:03:40 +0800
comments: true
categories: [hadoop]
---

## 前言

Partition发生在map之后，reduce之前，发生在执行map task节点的缓存中。 hadoop默认将map函数输出的key,value读入缓存(io.sort.mb控制，默认100MB)，读入过程中会将其放在不同>的分区中，缓存中数据量达到溢出与之后写入文件。默认的分区方式为对hash键(map输出的key,value对即为一个hash对)取模，取模规则为 key%numPartition.

**Hadoop默认的分区流程**:

```java
Job.class:

public class Job extends JobContextImpl implements JobContext { 
  public void setPartitionerClass(Class<? extends Partitioner> cls 
                                        ) throws IllegalStateException {
        ensureState(JobState.DEFINE);
        conf.setClass(PARTITIONER_CLASS_ATTR, cls, 
                          Partitioner.class);
      }   
}
```

**说明**：

`conf.setClass(PARTITIONER_CLASS_ATTR, cls, Partitioner.class)`方法，原型是

`Configuration.setClass(String name, Class<?> theClass, Class<?> xface)`，name是`property name`, theClass是

`property value`, xface是一个interface, named class必须能够实现xface这个接口

`PARTITIONER_CLASS_ATTR`实质上为：

```java
org.apache.hadoop.mapreduce.MRJobConfig.PARTITIONER_CLASS_ATTR= "mapreduce.job.partitioner.class"
```

`conf.setClass()`来自：

```java
public class JobContextImpl implements JobContext {
  protected final org.apache.hadoop.mapred.JobConf conf;
  @SuppressWarnings("unchecked")
    public Class<? extends Partitioner<?,?>> getPartitionerClass() 
    throws ClassNotFoundException {
      return (Class<? extends Partitioner<?,?>>) 
        conf.getClass(PARTITIONER_CLASS_ATTR, HashPartitioner.class);
    }
}
```

`HashPartitioner.class`来自：

```java
public class HashPartitioner<K, V> extends Partitioner<K, V> {  
  public int getPartition(K key, V value,
      int numReduceTasks) {
    return (key.hashCode() & Integer.MAX_VALUE) % numReduceTasks;
  }
}
```

key.hashCode()：

以Text.class为例,

```java
Text extends BinaryComparable{
  public int hashCode() {
    return super.hashCode();
  }
}
abstract class BinaryComparable
  implements Comparable<BinaryComparable> {
  @Override
  public int hashCode() {
    return WritableComparator.hashBytes(getBytes(), getLength());
  }
}
```

可见`hashCode()`方法其实最终来自`WritableComparator.class`：

```java
public class WritableComparator implements RawComparator {
  /** Compute hash for binary data. */
  public static int hashBytes(byte[] bytes, int offset, int length) {
    int hash = 1;
    for (int i = offset; i < offset + length; i++)
      hash = (31 * hash) + (int)bytes[i];
    return hash;
  }
                                                            
  /** Compute hash for binary data. */
  public static int hashBytes(byte[] bytes, int length) {
    return hashBytes(bytes, 0, length);
  }
}
```

实质上是：

```java
int hashBytes(bytes, 0, length) {
    int hash = 1;
    for (int i = 0; i < length; i++)
      hash = (31 * hash) + (int)bytes[i];
    return hash;
  }
}
```

从以上分析可以清晰地看到，hadoop分区机制默认行为是根据用户指定的某Writable类里定义的hashCode()方法来决定分区原则。

如果用户在Job.class里用`setPartitionerClass`定义了自己的分区类，则优先使用用户自定义的类。

以下自己写的案例就是基于自定义分区类的方法自主决定分区原则。


## 案例描述

以找有共同好友的users作为背景，现在将不同的users放入不同的分区。

**raw data**:

```
Tom:f1,f2,f3
Lucy:f4,f2,f5
Luciuz:f2,f4,f5
Ming:f8
Lili:f3
```

**map输出**：

```java
f1    Tom
f2    Tom
f3    Tom
f4    Lucy
f2    Lucy
f5    Lucy
f2    Luciuz
f4    Luciuz
f5    Luciuz
f8    Ming
f3    Lili
```

以map的value作为分区条件,定义5个分区：


```
Tom    =>    Partition0
Lucy   =>    Partition1
Luciuz =>    Partition2
others =>    Partition3
```

最后一个分区不存放信息,暂时性关闭Reducer，让其直接输出map后的结果。

**MapReduce程序**:

```java
import java.io.IOException;
import java.util.Map.Entry;
import java.util.HashMap;
import java.lang.Iterable;
                                                                                 
                                                                                 
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.util.Tool;
                                                                                 
public class FindUsersHaveSameFriends extends Configured implements Tool  {
                                                                                 
    static class PreMap extends Mapper<LongWritable, Text, Text, Text> {
        public void map(LongWritable key, Text val, Context context) 
          throws IOException, InterruptedException {
                                                                                 
            String line = val.toString();
            String user = line.split(":")[0];
            String[] friends = line.split(":")[1].split(",");
            for(String friend : friends) {      
                context.write(new Text(friend), new Text(user));
            }
        }
    }
                                                                                 
                                                                                 
    static class Reduce extends Reducer<Text, Text, Text, NullWritable> {
        HashMap<Text, Text> hash = new HashMap<Text, Text>();
        public void reduce(Text key, Iterable<Text> vals, Context context) 
          throws IOException, InterruptedException {
            //String friend = key.toString();
            int count = 0;
            String users = "";
            for(Text val : vals) {
                if(users.length() > 0) users += ",";
                users += val;
                count ++;
            }
                                                                                 
            if(count >= 2) hash.put(new Text(users), new Text("1"));
        }
                                                                                 
        @Override
        protected void cleanup(Context context) 
          throws IOException,InterruptedException {
                                                                                 
            for(Entry<Text,Text>entry : hash.entrySet()) {
                context.write(entry.getKey(), NullWritable.get());
            }
                                                                                 
        }
    }
                                                                                 
    static class Partition extends Partitioner<Text,Text> {
                                                                                 
            @Override
            public int getPartition(Text key, Text val, int numPartitions) {
                int result = 0;
                switch(val.toString()) {
                    case "Tom":
                        result = 0;
                        break;
                    case "Lucy":
                        result = 1;
                        break;
                    case "Luciuz":
                        result = 2;
                        break;
                    default:
                        result = 3;
                        break;
                }
                return result;
            }
    }
                                                                                 
    @Override
    public int run(String[] args) throws Exception {
                                                                                 
        Job job = Job.getInstance(getConf());
        job.setJarByClass(getClass());
                                                                                 
        job.setMapperClass(PreMap.class);
        //job.setReducerClass(Reduce.class);
        job.setPartitionerClass(Partition.class);;
                                                                                 
        job.setNumReduceTasks(5);
        job.setMapOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
                                                                                 
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
                                                                                 
        return job.waitForCompletion(true) ? 0 : 1;
    }
                                                                                 
                                                                                 
    public static void main(String[] args) 
      throws Exception {
        ToolRunner.run(new FindUsersHaveSameFriends(), args);
                                                                                 
    }
}


```

第65-87定义用户自己的`Partition.class`，第99行设定5个分区，第97行指定该Job使用`Partition.class`


**输出结果**:

```sh
% grep '\s' result/part-r-0000*
result/part-r-00000:f1  Tom
result/part-r-00000:f2  Tom
result/part-r-00000:f3  Tom
result/part-r-00001:f2  Lucy
result/part-r-00001:f4  Lucy
result/part-r-00001:f5  Lucy
result/part-r-00002:f2  Luciuz
result/part-r-00002:f4  Luciuz
result/part-r-00002:f5  Luciuz
result/part-r-00003:f3  Lili
result/part-r-00003:f8  Ming
% cat result/part-r-00004
%
```

可以看到，前三个文件中分别存储Tom，Lucy，Luciuz，第四个文件存放剩余Users，第五个分区为空。以上结果符合预期。

