---
layout: post
title: "ibus-pinyin词库增强"
date: 2014-02-03 00:00:21 +0800
comments: true
categories: [linux]
---
默认下，ibus-pinyin即CentOS自带的中文输入法默认词库只有3M:

```bash
% du -hs /usr/share/ibus-pinyin/db/android.db
3M    /usr/share/ibus-pinyin/db/android.db
```

如此会导致输入中文时匹配的准确度低的惊人，只有少部分自动匹配的词组是对的。

通过更新本地词库，可以大幅提高词组只能匹配的准确度：

```bash
wet http://hslinuxextra.googlecode.com/files/sougou-phrases-full.7z
```

安装7z來解压*7z文件：

```bash
% yum install p7*

% 7z x sougou-phrases-full.7z

```

更新词库:

```bash
cp -a ibus/ndroid.db /usr/share/ibus-pinyin/db/android.db
```

查看更新后词库的大小：

```bash
% du -hs /usr/share/ibus-pinyin/db/android.db
158M    /usr/share/ibus-pinyin/db/android.db
```
