---
layout: post
title: "python filter map reduce and lambda"
date: 2014-02-08 11:19:36 +0800
comments: true
categories: [python]
---

## 一.lambda

 def f (x): return x**2

 等价于

 g = lambda x: x**2

### 1.1 常规用法

```python
>>> (lambda x:x**2)(4)
16
>>> g = lambda x:x**2
>>> g(4)
16
>>> g = lambda x,y:x*y
>>> g(2,7)
14
>>> g = lambda x,y:(x*y, x, y)
>>> g(2,7)
(14, 2, 7)
```

### 1.2 动态构造函数

```python
>>> def make_incrementor (n): return lambda x: x+n
... 
>>> def make_incrementor (n): return lambda x: x+
KeyboardInterrupt
>>> f1 = make_incrementor(2)
>>> f2 = make_incrementor(8)
>>> print f1(10), f2(10)
```

### 1.3 在map, reduce, filter中使用

```python
>>> help(filter)
filter(...)
    filter(function or None, sequence) -> list, tuple, or string
>>> list = [2, 18, 9, 22, 17, 24, 8, 12, 27]
>>> print filter(lambda x: x % 9 == 0,list)
[18, 9, 27]
>>> def f(x): return x % 9 == 0
... 
>>> print filter(f,list)
[18, 9, 27]
>>> 
>>> print map(lambda x: x * 2 + 10, list)
[14, 46, 28, 54, 44, 58, 26, 34, 64]
>>> def f(x):return x * 2 + 10
... 
>>> print map(f, list)
[14, 46, 28, 54, 44, 58, 26, 34, 64]
>>> 
>>> print reduce(lambda x, y: x + y, list)
139
>>> def f(x,y):return x + y
... 
>>> print reduce(f, list)
139
```
### 1.4 举例

例1. 找质数

```python
>>> numbs = range(2,100)
>>> for i in range(2,8):
...     numbs = filter(lambda x : x == i or x % i, numbs)
... 
>>> print numbs
[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
```
例2. 统计单词个数

```python
>>> sentence = 'It is raining cats and dogs'
>>> words = sentence.split()
>>> print words
['It', 'is', 'raining', 'cats', 'and', 'dogs']
>>> langths = map(lambda word : len(word), words)
>>> print langths
[2, 2, 7, 4, 3, 4]
```

单行模式：

```python
>>> print map(lambda w: len(w), 'It is raining cats and dogs'.split())
[2, 2, 7, 4, 3, 4]
```

例3.  调用UNIX命令并处理

```python
>>> import commands
>>> 
>>> mount = commands.getoutput('mount -v')
>>> lines = mount.splitlines()
>>> points = map(lambda line: line.split()[2], lines)
>>> 
>>> print points
['/', '/var', '/usr', '/usr/local', '/tmp', '/proc']
```

单行模式：

```python
print map(lambda x: x.split()[2], commands.getoutput('mount -v').splitlines())
['/', '/var', '/usr', '/usr/local', '/tmp', '/proc']
```

注意: mount是一个大string, lines是一个list, split()按空格分割，split()[2]得到分割后的list里的第三个元素

补充,split的用法

```python
>>> print line
sysfs on /sys type sysfs (rw)
>>> print line.split('/')
['sysfs on ', 'sys type sysfs (rw)']
>>> print line.split('/')[1]
sys type sysfs (rw)
```

## 二. filter

filter(function, sequence)：对sequence中的item依次执行function(item)，将执行结果为True的item组成一个List/String/Tuple（取决于sequence的类型）

返回： function的返回值只能是True或False

把sequence中的值逐个当参数传给function，如果function(x)的返回值是True，就把x加到filter的返回值里面。一般来说filter的返回值是list，特殊情况如sequence是string或tuple，则返回值按照sequence的类型。

```python
>>> def f(x): return x % 2 != 0 and x % 3 != 0 
>>> filter(f, range(2, 25)) 
[5, 7, 11, 13, 17, 19, 23]
>>> def f(x): return x != 'a' 
>>> filter(f, "abcdef") 
'bcdef'
```

例子：

找出1到10之间的奇数

`filter(lambda x:x%2!=0,range(1,11))`

返回值: [1,3,5,7,9]
 
如果sequence是一个string
filter(lambda x:len(x)!=0,'hello')返回'hello'
filter(lambda x:len(x)==0,'hello')返回''

## 三. map

map(function, sequence) ：对sequence中的item依次执行function(item)，见执行结果组成一个List返回：

把sequence中的值当参数逐个传给function，返回一个包含函数执行结果的list。
如果function有两个参数，即map(function,sequence1,sequence2)。
 

```python
>>> def cube(x): return x*x*x 
>>> map(cube, range(1, 11)) 
[1, 8, 27, 64, 125, 216, 343, 512, 729, 1000]
>>> def cube(x) : return x + x 
... 
>>> map(cube , "abcde") 
['aa', 'bb', 'cc', 'dd', 'ee']
另外map也支持多个sequence，这就要求function也支持相应数量的参数输入：
>>> def add(x, y): return x+y 
>>> map(add, range(8), range(8)) 
[0, 2, 4, 6, 8, 10, 12, 14]
```

例子：

求1*1,2*2,3*3,4*4

`map(lambda x:x*x,range(1,5))`

返回值是[1,4,9,16]
 
## 四. reduce

reduce(function, sequence, starting_value)：对sequence中的item顺序迭代调用function，如果有starting_value，还可以作为初始值调用，例如可以用来对List求和：

function接收的参数个数只能为2

先把sequence中第一个值和第二个值当参数传给function，再把function的返回值和第三个值当参数传给
function，然后只返回一个结果。

```python
>>> def add(x,y): return x + y 
>>> reduce(add, range(1, 11)) 
55 （注：1+2+3+4+5+6+7+8+9+10）
>>> reduce(add, range(1, 11), 20) 
75 （注：20+1+2+3+4+5+6+7+8+9+10）
```

例子：
求1到10的累加

`reduce(lambda x,y:x+y,range(1,11))`

返回值是55。

## 五. merge filter, map, reduce, lambda 

```python
kmpathes = filter(lambda kmpath: kmpath,                  
map(lambda kmpath: string.strip(kmpath),
string.split(l, ':')))
```
