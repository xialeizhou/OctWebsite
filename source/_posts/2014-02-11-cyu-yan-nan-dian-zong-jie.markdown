---
layout: post
title: "C语言难点总结"
date: 2014-02-11 12:15:43 +0800
comments: true
categories: [c]
---

##  函数指针(function pointer)

函数指针是指向函数的指针变量。 因而“函数指针”本身首先应是指针变量，只不过该指针变量指向函数。 C在编译时，每一个函数都有一个入口地址，该入口地址就是函数指针所指向的地址。因此在任何需要函数指针作为输入的地方均可以直接使用函数名。 

函数指针有两个用途：调用函数和做函数的参数

例1 (无参，无返回值的函数指针)

```c
#include <stdio.h>
 
// function prototype
void sayHello();
 
// function implementation
void sayHello() {
  printf("hello world\n");
}
 
// calling from main

int main() {
  void (*funcPtr)();
  funcPtr = sayHello;
  
  void (*sayHelloPtr)() = sayHello;
  (*sayHelloPtr)();
  sayHelloPtr();
}
```

例2(有参的函数指针)

```c
#include <stdio.h>
 
// function prototype
void subtractAndPrint(int x, int y);
 
// function implementation
void subtractAndPrint(int x, int y) {
  int z = x - y;
  printf("Simon says, the answer is: %d\n", z);
}
 
// calling from main
int main() {
  void (*sapPtr)(int, int) = subtractAndPrint;
  (*sapPtr)(10, 2);
  sapPtr(10, 2);
}
```

例3(有参,有返回值的函数指针)

```c
#include <stdio.h>
 
// function prototype
int subtract(int x, int y);
 
// function implementation
int subtract(int x, int y) {
  return x - y;
}
 
// calling from main
int main() {
  int (*subtractPtr)(int, int) = subtract;
 
  int y = (*subtractPtr)(10, 2);
  printf("Subtract gives: %d\n", y);
 
  int z = subtractPtr(10, 2);
  printf("Subtract gives: %d\n", z);
}
```

例4(函数指针作为参数)

```c
#include <stdio.h>
 
// function prototypes
int add(int x, int y);
int subtract(int x, int y);
int domath(int (*mathop)(int, int), int x, int y);
 
// add x + y
int add(int x, int y) {
  return x + y;
}
 
// subtract x - y
int subtract(int x, int y) {
  return x - y;
}
 
// run the function pointer with inputs
int domath(int (*mathop)(int, int), int x, int y) {
  return (*mathop)(x, y);
}
 
// calling from main
int main() {
 
  // call math function with add
  int a = domath(add, 10, 2);
  printf("Add gives: %d\n", a);
 
  // call math function with subtract
  int b = domath(subtract, 10, 2);
  printf("Subtract gives: %d\n", b);
}
```

例5(函数名和函数地址)

```c
#include <stdio.h>
 
// function prototypes
void add(char *name, int x, int y);
 
// add x + y
void add(char *name, int x, int y) {
  printf("%s gives: %d\n", name, x + y);
}
 
// calling from main
int main() {
 
  // some funky function pointer assignment
  void (*add1Ptr)(char*, int, int) = add;
  void (*add2Ptr)(char*, int, int) = *add;
  void (*add3Ptr)(char*, int, int) = &add;
  void (*add4Ptr)(char*, int, int) = **add;
  void (*add5Ptr)(char*, int, int) = ***add; 
 
  // execution still works
  (*add1Ptr)("add1Ptr", 10, 2);
  (*add2Ptr)("add2Ptr", 10, 2);
  (*add3Ptr)("add3Ptr", 10, 2);
  (*add4Ptr)("add4Ptr", 10, 2);
  (*add5Ptr)("add5Ptr", 10, 2);
 
  // this works too
  add1Ptr("add1PtrFunc", 10, 2);
  add2Ptr("add2PtrFunc", 10, 2);
  add3Ptr("add3PtrFunc", 10, 2);
  add4Ptr("add4PtrFunc", 10, 2);
  add5Ptr("add5PtrFunc", 10, 2);
}
```

##  [C中的内存地址基础](http://denniskubes.com/2012/08/17/basics-of-memory-addresses-in-c/)

## [The 5-minute Guide to C Pointers](http://denniskubes.com/2012/08/16/the-5-minute-guide-to-c-pointers/)

## [Do you actually know what *p++ does in C?](http://denniskubes.com/2012/08/14/do-you-know-what-p-does-in-c/)

## [Is C Pass by Value or Reference?](http://denniskubes.com/2012/08/20/is-c-pass-by-value-or-reference/)

