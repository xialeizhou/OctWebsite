---
layout: post
title: "python基础编程知识总结(一)"
date: 2014-02-04 18:02:42 +0800
comments: true
categories: [python]
---
正则表达式，目录遍历，字典/列表等的使用, 文件读写，系统命令调用

# 提纲

1. 正则表达式 

2. 遍历目录方法 

3. 列表按列排序 

4. 去重 

5. 字典排序 

6. 字典 

7. 列表 

8. 字符串互转 

9. 时间对象操作 

10. 命令行参数解析 

11.print格式化输出

12. 进制转换 

13. 调用*系统命令*或*外部脚本* 

14. 读写文件


# 正文

## 一. 正则表达式

python的正则表达式支持有两种，一种是“module-level functions"， 另一种是"RegexObject methods”.

注意:

"module-level functions"是一种快捷实现，不许要预先编译，但是较RegexObject methods而言，缺少一些调优的参数支持。


##1.1 module-level functions

"module-module functions" 主要包含三类，分别是functions, constants(大写)，以及一个exception.

###  1.1.1 re.compile(pattern, flags=0)

用法：

```python
obj = re.compile(pattern)
result = obj.match(string)
```

等价于RegexObject methods:

```python
result = re.match(pattern, string)
```

以下两个functions将pattern 即RE字符串作为第一个参数先产生一个RegexOject，然后在这个RegexOject上调用相应方法

### 1.1.2 re.search(pattern, string, flags=0)

返回 a corresponding MatchObject instance

### 1.1.3 re.match(pattern, string, flags=0)

注意1 match()从0处开始匹陪，并且只报告一次，开始匹配不是从0开始，即使后边匹配到，match()也会返回None.

举例：

```python
>>> print re.match('super', 'superstition')
<_sre.SRE_Match object at 0xb7699058>   # 匹配从0开始
>>> print re.match('super', 'isuperstition')
None                                    # 匹配到了，但不是从0处开始
```

注意2 即使在MULTILINE模式下，`re.match()`依然只匹配最开始0处（仅一次），而不会匹配每一行的开始处。


search()和match()的区别

举例1

关键词： 

search()  -- "anywhere" 

match()  -- "begining of the string"

```python
>>> re.match("c", "abcdef")  # No match
>>> re.search("c", "abcdef") # Match
<_sre.SRE_Match object at ...>
```

**举例2**

match()和search()的等价

```python
>>> re.match("a", "abcdef")  # Match "a"
# 等价于...
>>> re.match("^a", "abcdef") # Match "a"
```

**举例3**

即使在MULTILINE模式下，match()也只在string开始0处匹配一次，而使用'^', search()可以匹配每一行首

```python
>>> re.match('X', 'A\nB\nX', re.MULTILINE)  # No match
>>> re.search('^X', 'A\nB\nX', re.MULTILINE)  # Match
<_sre.SRE_Match object at ...>
```

### 1.1.4 re.split(pattern, string, maxsplit=0, flags=0)**

基本的语法为:

(1).  以最大分隔数maxsplit来分割，返回的list的最后一个值为分割后剩下的。

因此，如果maxsplit=0, 则以最大数分割，剩余的string自然是空的''

(2).  若出现**捕获型括号**，则模式分组中的捕获的元素也全部输出。输出方式为，被分割的元素和模式
分组中的text**相间**输出

```python
>>> re.split('\W+', 'Words, words, words.') # 注意\W大写
['Words', 'words', 'words', '']
>>> re.split('(\W+)', 'Words, words, words.')
['Words', ', ', 'words', ', ', 'words', '.', '']
>>> re.split('\W+', 'Words, words, words.', 1)
['Words', 'words, words.']
>>> re.split('[a-f]+', '0a3B9', flags=re.IGNORECASE)
['0', '3', '9']
```

(3). 注意字符串开始处为分割符中的捕获组出现在string的开始出，则分割后的list会以empty string开始。

```python
>>> re.split('(\W+)', '...words, words...')
['', '...', 'words', ', ', 'words', '...', '']
```

(4). 匹配不到，则返回整个string

```python
>>> re.split('x*', 'foo')
['foo']
>>> re.split("(?m)^$", "fo
```

### 1.1.5 re.findall(pattern, string, flags=0)**

返回一个**列表**

以**列表**的形式返回所有匹配到的字符串

```python
>> print re.findall(r'\w+', line, re.I)
['Words', 'words', 'words']
```
注意： **The string is scanned left-to-right, and matches are returned in the order found.**

### 1.1.6  re.finditer(pattern, string, flags=0)

返回一个match object 的**迭代器**(iterator), 该迭代器会返回每一个匹配结果

```python
>>> iter= re.finditer(r'\D+', 'one1two2three3four4')
>>> for m in iter:
...     print m.group(),
... 
one two three four
```

**理解**： 正则引擎在string里从左往右搜索RE object，每次匹配到RE则将match object放入一个虚拟的list里
搜索完毕后将这个装有match object1, match object2, ...的list对象返回

### 1.1.7 re.sub(pattern, repl, string, count=0, flags=0)**

## 1.2  constants

match 语法： `re.match(pattern, string, flags=0)`

search 语法： `re.search(pattern, string, flags=0) `

以上两个基本的functions中的flags类型总结如下：

**re.I** （大小写不敏感，re.IGNORECASE, 当flags设置位LOCALE时无效)

**re.L** (re.LOCALE, 对 \w, \W, \b, \B, \s 和 \S起作用）
  
  以\w来举例：

  当LOCALE和UNICODE flags没有设定时，\w表示匹配alphanumber 类型的character 和 underscore, 即[a-zA-Z0-9_].

  当flags设定为**LOCALE**时，\w 等价于[0-9_]加上当前LOCALE下任意定义为alphanumber的characters.
 
  当flags设定为**UNICODE**时，\w等价于[0-9_]加上在Unicode字符集里任意被归类为alphanumberic的characters.

**re.M**  (re.MULTILINE, 类似perl里的/m)

**re.S** (re.DOALL, 对“.”使用，使它可以匹配换行符)

**re.U** (re.UNICODE, Make \w, \W, \b, \B, \d, \D, \s and \S dependent on the Unicode character properties database.**New in version 2.0**）

**re.X** （re.VERBOSE， 允许正则跨行，也允许注释）


## 1.3  Module Contents(except flags)

`re.compile(pattern, flags=0)`

编译一个regular expression pattern 使其成为一个regular expression object，这个对象可以被match()和search()用来匹配.

`re.DEBUG` Display debug information about compiled expression.

`re.search(pattern, string, flags=0)`

`re.match(pattern, string, flags=0)` (注意在re.M模式下，re.match()只匹配**字符串**开头，不匹配**行**首）

`re.split(pattern, string, maxsplit=0, flags=0)` 
  
```python
 >>> re.split('\W+', 'Words, words, words.')
 ['Words', 'words', 'words', '']
 >>> re.split('(\W+)', 'Words, words, words.')
 ['Words', ', ', 'words', ', ', 'words', '.', '']
 >>> re.split('\W+', 'Words, words, words.', 1)
 ['Words', 'words, words.']
 >>> re.split('[a-f]+', '0a3B9', flags=re.IGNORECASE)
 ['0', '3', '9']
```
## 二. 遍历目录方法

```python  walking_dir.python
#!/usr/bin/env python
#coding=utf-8
import os
fileList = []
rootdir = "/home/luciuz/Code"
for root, subFolders, files in os.walk(rootdir):
if '.git' in subFolders: subFolders.remove('.git')  # 排除特定目录
for file in files:
  if file.find(".c") != -1:# 查找特定扩展名的文件
      file_dir_path = os.path.join(root,file)
      fileList.append(file_dir_path)  

print fileList
```
os.walk的用法：

```python
>>> help(os)
walk(top, topdown=True, onerror=None, followlinks=False)
Directory tree generator.
 
   For each directory in the directory tree rooted at top (including top
   itself, but excluding '.' and '..'), yields a 3-tuple
 
     dirpath, dirnames, filenames

   dirpath is a string, the path to the directory.  dirnames is a list of
   the names of the subdirectories in dirpath (excluding '.' and '..').
   filenames is a list of the names of the non-directory files in dirpath.
   Note that the names in the lists are just names, with no path components.
   To get a full path (which begins with top) to a file or directory in
   dirpath, do os.path.join(dirpath, name).
```

## 三.  列表按列排序(list sort)

```python
>>> help(sorted)
sorted(...)
    sorted(iterable, cmp=None, key=None, reverse=False) --> new sorted list
>>> array = [('2013-02-18', 'c', '312.12'),('1932-21-21','a','123.21'), ('1998-12-21','f','92.58')]
>>> print array[1][1]
a
>>> b = sorted(array,key=lambda s:s[1], reverse=False)
>>> print b
[('1932-21-21', 'a', '123.21'), ('2013-02-18', 'c', '312.12'), ('1998-12-21', 'f', '92.58')]
>>> b = sorted(array,key=lambda s:s[1], reverse=True)
>>> print b
[('1998-12-21', 'f', '92.58'), ('2013-02-18', 'c', '312.12'), ('1932-21-21', 'a', '123.21')]
>>> b = sorted(array,key=lambda s:s[0], reverse=True)
>>> print b
[('2013-02-18', 'c', '312.12'), ('1998-12-21', 'f', '92.58'), ('1932-21-21', 'a', '123.21')]
>>> b = sorted(array,key=lambda s:s[0], reverse=False)
>>> print b
[('1932-21-21', 'a', '123.21'), ('1998-12-21', 'f', '92.58'), ('2013-02-18', 'c', '312.12')]
```

## 四. 列表去重(list uniq)

```python
>>> lst= [(1,'sss'),(2,'fsdf'),(1,'sss'),(3,'fd')]
>>> set(lst)
set([(2, 'fsdf'), (3, 'fd'), (1, 'sss')])
>>>
>>> lst = [1, 1, 3, 4, 4, 5, 6, 7, 6]
>>> set(lst)
set([1, 3, 4, 5, 6, 7])
```

## 五. 字典排序(dict sort)

假设按字典的value排序：

```python
>>> from operator import itemgetter
>>> aa = {"a":"1","sss":"2","ffdf":'5',"ffff2":'3'}
>>> sort_aa = sorted(aa.items(),key=itemgetter(1))
>>> sort_aa
[('a', '1'), ('sss', '2'), ('ffff2', '3'), ('ffdf', '5')]
```

## 六. 字典,列表,字符串互转

以下是生成数据库连接字符串,从字典转换到字符串

```python
>>> params = {"server":"mpilgrim", "database":"master", "uid":"sa", "pwd":"secret"}
>>> ["%s=%s" % (k, v) for k, v in params.items()]
['server=mpilgrim', 'uid=sa', 'database=master', 'pwd=secret']
>>> ";".join(["%s=%s" % (k, v) for k, v in params.items()])
'server=mpilgrim;uid=sa;database=master;pwd=secret'
```
下面的例子 是将字符串转化为字典

```python
>>> a = 'server=mpilgrim;uid=sa;database=master;pwd=secret'
>>> aa = {}
>>> for i in a.split(';'):aa[i.split('=',1)[0]] = i.split('=',1)[1]
...
>>> aa
{'pwd': 'secret', 'database': 'master', 'uid': 'sa', 'server': 'mpilgrim'}
```

-----------------------------------------------------------------
参考:

[python编程中常用的12种基础知识总结](http://www.aikaiyuan.com/5315.html)
